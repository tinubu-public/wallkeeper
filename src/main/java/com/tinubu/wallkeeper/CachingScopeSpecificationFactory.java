/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.tinubu.commons.ddd2.domain.specification.Specification;

/**
 * Caching {@link ScopeSpecificationFactory} indexed on scope.
 *
 * @param <T> object type
 * @param <SC> scope context type
 * @param <S> user scope type
 */
public abstract class CachingScopeSpecificationFactory<T, SC extends ScopeContext, S extends UserScope>
      implements ScopeSpecificationFactory<T, SC, S> {

   private static final int CACHE_MAX_SIZE = 500;
   private static final Duration CACHE_MAX_DURATION = Duration.ofSeconds(300);

   /**
    * Cache specification for each (context, scope). We use "expire after write" cache to ensure scope is
    * refreshed after the configured time, to control how much time a scope is inconsistent.
    */
   private final LoadingCache<SpecificationCacheKey, Specification<T>> scopeSpecificationCache;

   public CachingScopeSpecificationFactory(int cacheMaxSize, Duration cacheMaxDuration) {
      notNull(cacheMaxDuration, "cacheMaxDuration");

      this.scopeSpecificationCache = CacheBuilder
            .newBuilder()
            .maximumSize(cacheMaxSize)
            .expireAfterWrite(cacheMaxDuration.getSeconds(), TimeUnit.SECONDS)
            .build(new CacheLoader<SpecificationCacheKey, Specification<T>>() {
               @Override
               public Specification<T> load(final SpecificationCacheKey key) {
                  return buildScopeSpecification(key.context, key.userScope);
               }
            });
   }

   public CachingScopeSpecificationFactory() {
      this(CACHE_MAX_SIZE, CACHE_MAX_DURATION);
   }

   /**
    * Invalidate cache for all (users, context).
    */
   public void invalidateAll() {
      scopeSpecificationCache.invalidateAll();
   }

   /**
    * Invalidate cache for specified (users, context).
    */
   public void invalidateSpecification(SC context, Optional<S> userScope) {
      notNull(userScope, "userScope");

      scopeSpecificationCache.invalidate(new SpecificationCacheKey(context, userScope));
   }

   public Specification<T> scopeSpecification(SC context, Optional<S> userScope) {
      notNull(userScope, "userScope");

      try {
         return scopeSpecificationCache.get(new SpecificationCacheKey(context, userScope));
      } catch (ExecutionException e) {
         throw new IllegalStateException(e);
      }
   }

   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   protected abstract Specification<T> buildScopeSpecification(SC context, Optional<S> scope);

   /**
    * Defines specification cache key.
    */
   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   private class SpecificationCacheKey {
      private final SC context;
      private final Optional<S> userScope;

      public SpecificationCacheKey(SC context, Optional<S> userScope) {
         this.context = context;
         this.userScope = notNull(userScope, "userScope");
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         SpecificationCacheKey that = (SpecificationCacheKey) o;
         return Objects.equals(context, that.context) && Objects.equals(userScope, that.userScope);
      }

      @Override
      public int hashCode() {
         return Objects.hash(context, userScope);
      }
   }

}
