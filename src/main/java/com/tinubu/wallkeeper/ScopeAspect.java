/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static java.util.stream.Collectors.toList;
import static org.springframework.core.annotation.AnnotationUtils.findAnnotation;
import static org.springframework.core.annotation.AnnotationUtils.getAnnotation;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.core.MethodParameter;
import org.springframework.core.ResolvableType;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;

import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.wallkeeper.condition.ConditionFunctions;

@Component
@Aspect
// FIXME @Scoped(ignored = true) annotated object in a List/Iterable ignoré ? The same for Specification ? for Stream ?
// FIXME resolver apply in Stream (parameter and returnObject) -> can't change stream type from inside !
// FIXME Stream scopé en paramètre retourné par la fonction et scopé également -> instrumenté (peek) 2 fois ?
public class ScopeAspect {
   private static final Logger log = LoggerFactory.getLogger(ScopeAspect.class);

   /** Pre-instanciates this resolvable type. */
   private static final ResolvableType OPTIONAL_TYPE = ResolvableType.forClass(Optional.class);
   /** Pre-instanciates this resolvable type. */
   private static final ResolvableType ITERABLE_TYPE = ResolvableType.forClass(Iterable.class);
   /** Pre-instanciates this resolvable type. */
   private static final ResolvableType STREAM_TYPE = ResolvableType.forClass(Stream.class);
   /** Pre-instanciates this resolvable type. */
   private static final ResolvableType SPECIFICATION_TYPE = ResolvableType.forClass(Specification.class);

   private static final Scoped DEFAULT_SCOPED_ANNOTATION = DefaultScopedHolder.defaultScoped();

   private final ApplicationContext ctx;
   private final ScopeService<ScopeContext, UserScope> scopeService;

   /**
    * Global SpEL expression cache, indexed by expression string.
    */
   private final Map<String, Expression> expressionCache = new ConcurrentHashMap<>();

   public ScopeAspect(ApplicationContext ctx, ScopeService<ScopeContext, UserScope> scopeService) {
      this.ctx = ctx;
      this.scopeService = scopeService;
   }

   /**
    * Scope check is triggered by {@link Scoped} annotation either on :
    * <ul>
    *    <li>A class, to check all public methods return values</li>
    *    <li>A method, to check method return value</li>
    *    <li>A method parameter (in method signature), to check parameter</li>
    * </ul>
    * Whatever the annotation position, only public methods will be checked for scope.
    *
    * @param joinPoint aspect join point
    *
    * @return cut method return value
    *
    * @throws ParameterScopeException if scope check fails for a parameter
    * @throws ReturnObjectScopeException if scope check fails for returned object
    * @throws Throwable if original operation throws any exception
    */
   @Around(
         "execution(public * *(..)) && (within(@ScopedBean *) || @annotation(Scoped) || @annotation(ScopedSet) || execution(* *(.., @Scoped (*), ..))) || execution(* *(.., @ScopedSet (*), ..)))")
   public Object checkScope(ProceedingJoinPoint joinPoint) throws Throwable {
      final StopWatch watch = new StopWatch();

      watch.start();

      Method method = resolveUserMethod(joinPoint);
      List<ParameterModel> parameters = parameters(joinPoint);

      String methodDisplayName = resolveUserClass(joinPoint).getName() + "::" + method.getName();

      for (ParameterModel parameter : parameters) {
         checkParameterScope(parameter, method, methodDisplayName, parameters);
      }

      watch.suspend();
      Object returnObject = joinPoint.proceed(proceedingParameters(parameters));
      watch.resume();

      returnObject = checkReturnObjectScope(returnObject, method, methodDisplayName, parameters);

      if (log.isDebugEnabled()) {
         log.debug("[Scope] Checked scope successfully for '{}' in {}ms",
                   methodDisplayName,
                   watch.getTime(TimeUnit.MILLISECONDS));
      }

      return returnObject;
   }

   private void checkParameterScope(ParameterModel parameter,
                                    Method method,
                                    String methodDisplayName,
                                    List<ParameterModel> parameters) {
      if (parameter.value() != null || Specification.class.isAssignableFrom(parameter.type())) {
         for (Scoped scopedParameter : parameterScopedAnnotations(parameter)) {
            boolean scopeIgnored = this
                  .<Boolean>evaluateExpression(parameters,
                                               scopeIgnoredExpression(scopedParameter, method),
                                               new ScopedRoot(parameter.value(), false))
                  .orElse(false);

            if (!scopeIgnored) {
               ScopeContext scopeContext = this
                     .<ScopeContext>evaluateExpression(parameters,
                                                       scopeContextExpression(scopedParameter, method),
                                                       new ScopedRoot(parameter.value(), false))
                     .orElse(null);
               String scopeResolverExpression = scopeResolverExpression(scopedParameter, method);

               if (Specification.class.isAssignableFrom(parameter.type())) {
                  updateSpecificationParameterWithScope(parameter, methodDisplayName, scopeContext);
               } else if (Stream.class.isAssignableFrom(parameter.type())) {
                  updateStreamParameterWithScope(parameter,
                                                 methodDisplayName,
                                                 scopeContext,
                                                 scopeResolverExpression,
                                                 parameters);
               } else {

                  Object resolvedParameterValue =
                        resolveScopedObject(parameter.value(), scopeResolverExpression, parameters, false);

                  if (log.isDebugEnabled()) {
                     log.debug(
                           "[Scope] Check scope for '{}' parameter of '{}' operation using '{}' scope context",
                           parameterString(parameter.name(), parameter.value(), resolvedParameterValue),
                           methodDisplayName,
                           scopeContext);
                  }

                  if (!scopeService.inScope(resolvedParameterValue, scopeContext, true)) {
                     throw new ParameterScopeException(parameter.name(),
                                                       resolvedParameterValue,
                                                       String.format(
                                                             "Scope check failed for '%s' parameter of '%s' operation using '%s' scope context",
                                                             parameterString(parameter.name(),
                                                                             parameter.value(),
                                                                             resolvedParameterValue),
                                                             methodDisplayName,
                                                             scopeContext));
                  }
               }
            }
         }
      }
   }

   private Object checkReturnObjectScope(Object returnObject,
                                         Method method,
                                         String methodDisplayName,
                                         List<ParameterModel> parameters) {

      if (returnObject != null) {
         List<Scoped> scopedSet = methodScopedAnnotation(method);

         if (scopedSet.isEmpty() && scopedBean(method)) {
            scopedSet.add(DEFAULT_SCOPED_ANNOTATION);
         }

         for (Scoped scopedReturn : scopedSet) {
            boolean scopeIgnored = this
                  .<Boolean>evaluateExpression(parameters,
                                               scopeIgnoredExpression(scopedReturn, method),
                                               new ScopedRoot(returnObject, true))
                  .orElse(false);

            if (!scopeIgnored) {
               ScopeContext scopeContext = this
                     .<ScopeContext>evaluateExpression(parameters,
                                                       scopeContextExpression(scopedReturn, method),
                                                       new ScopedRoot(returnObject, true))
                     .orElse(null);

               String scopeResolverExpression = scopeResolverExpression(scopedReturn, method);

               if (Stream.class.isAssignableFrom(returnObject.getClass())) {
                  returnObject = updateStreamReturnObjectWithScope((Stream<?>) returnObject,
                                                                   method,
                                                                   methodDisplayName,
                                                                   scopeContext,
                                                                   scopeResolverExpression,
                                                                   parameters);
               } else {
                  Object resolvedReturnObject =
                        resolveScopedObject(returnObject, scopeResolverExpression, parameters, true);

                  if (log.isDebugEnabled()) {
                     log.debug(
                           "[Scope] Check scope for '{}' return object of '{}' operation using '{}' scope context",
                           returnObjectString(returnObject, resolvedReturnObject),
                           methodDisplayName,
                           scopeContext);
                  }

                  if (!scopeService.inScope(resolvedReturnObject, scopeContext, true)) {
                     throw new ReturnObjectScopeException(resolvedReturnObject,
                                                          String.format(
                                                                "Scope check failed for '%s' return object of '%s' operation using '%s' scope context",
                                                                returnObjectString(returnObject,
                                                                                   resolvedReturnObject),
                                                                methodDisplayName,
                                                                scopeContext));
                  }
               }
            }
         }
      }

      return returnObject;
   }

   /**
    * Resolves the scoped object by applying the specified expression. If scoped object is an {@link Optional}
    * or {@link Iterable}, the resulting object is a container of the same kind, with items resolved.
    * The resolver expression must resolve container items types, and not the container itself.
    *
    * @param value value to resolve
    * @param scopeResolverExpression resolver expression
    * @param parameters method parameters used to evaluate expression
    *
    * @return resolved object
    */
   private Object resolveScopedObject(Object value,
                                      String scopeResolverExpression,
                                      List<ParameterModel> parameters,
                                      boolean isReturnObject) {
      if (value instanceof Optional) {
         return ((Optional<?>) value).map(v -> resolveScopedObject(v,
                                                                   scopeResolverExpression,
                                                                   parameters,
                                                                   isReturnObject));
      } else if (value instanceof Iterable) {
         return StreamSupport
               .stream(((Iterable<?>) value).spliterator(), false)
               .map(v -> resolveScopedObject(v, scopeResolverExpression, parameters, isReturnObject))
               .collect(toList());
      } else {
         return evaluateExpression(parameters,
                                   scopeResolverExpression,
                                   new ScopedRoot(value, isReturnObject)).orElse(value);
      }
   }

   /**
    * Builds a representation string for return object.
    *
    * @param returnObject return object value
    * @param resolvedReturnObject resolved return object value
    *
    * @return representation string for return object
    */
   private String returnObjectString(Object returnObject, Object resolvedReturnObject) {
      return returnObject + (!resolvedReturnObject.equals(returnObject) ? "->" + resolvedReturnObject : "");
   }

   /**
    * Builds a representation string for parameter.
    *
    * @param parameterName parameter name
    * @param parameterValue parameter value
    * @param resolvedParameterValue resolved parameter value
    *
    * @return representation string for return object
    */
   private String parameterString(String parameterName,
                                  Object parameterValue,
                                  Object resolvedParameterValue) {
      return parameterName + "=" + parameterValue + (!resolvedParameterValue.equals(parameterValue)
                                                     ? "->"
                                                       + resolvedParameterValue
                                                     : "");
   }

   /**
    * Returns {@code true} if method is contained in {@link ScopedBean} annotated class.
    *
    * @param method method to check
    *
    * @return {@code true} if method is contained in {@link ScopedBean} annotated class
    */
   private boolean scopedBean(Method method) {
      return scopedBean(method.getDeclaringClass());
   }

   /**
    * Returns {@code true} if class is {@link ScopedBean} annotated.
    *
    * @param beanClass bean class to check
    *
    * @return {@code true} if class is {@link ScopedBean} annotated
    */
   private boolean scopedBean(Class<?> beanClass) {
      return getAnnotation(beanClass, ScopedBean.class) != null;
   }

   /**
    * Updates {@link Specification} parameter by and'ing the scope specification to the original specification
    * before proceeding to method.
    *
    * @param parameter specification parameter to update
    * @param methodDisplayName current method display name
    * @param scopeContext scope context for parameter
    */
   @SuppressWarnings({ "unchecked", "rawtypes" })
   private void updateSpecificationParameterWithScope(ParameterModel parameter,
                                                      String methodDisplayName,
                                                      ScopeContext scopeContext) {
      if (log.isDebugEnabled()) {
         log.debug(
               "[Scope] Update {} '{}' parameter of '{}' operation with scope specification using '{}' scope context",
               parameter.type().getSimpleName(),
               parameter.name(),
               methodDisplayName,
               scopeContext);
      }

      Class<?> resolveParameterScopedClass = resolveParameterScopedClass(parameter);
      if (parameter.value() != null) {
         parameter.updateValue(scopeService.scopedSpecification((Specification) parameter.value(),
                                                                resolveParameterScopedClass,
                                                                scopeContext));
      } else {
         parameter.updateValue(scopeService
                                     .scopeSpecification(resolveParameterScopedClass, scopeContext)
                                     .orElseThrow(() -> new NotRegisteredScopeException(
                                           resolveParameterScopedClass)));
      }
   }

   /**
    * Updates {@link Stream} parameter by instrumenting stream with scope checking of each element at read
    * time.
    *
    * @param parameter specification parameter to update
    * @param methodDisplayName current method display name
    * @param scopeContext scope context for parameter
    * @param scopeResolverExpression scope resolved expression
    */
   @SuppressWarnings("rawtypes")
   private void updateStreamParameterWithScope(ParameterModel parameter,
                                               String methodDisplayName,
                                               ScopeContext scopeContext,
                                               String scopeResolverExpression,
                                               List<ParameterModel> parameters) {
      if (log.isDebugEnabled()) {
         log.debug(
               "[Scope] Instrument {} '{}' parameter of '{}' operation with scope checking using '{}' scope context",
               parameter.type().getSimpleName(),
               parameter.name(),
               methodDisplayName,
               scopeContext);
      }

      final AtomicReference<Optional<? extends Specification<?>>> unresolvedObjectScopeSpecification =
            new AtomicReference<>();

      parameter.updateValue(((Stream<?>) parameter.value()).peek(element -> {
         Object resolvedElement = resolveScopedObject(element, scopeResolverExpression, parameters, false);

         Specification scopeSpecification = parameterStreamElementSpecification(resolvedElement,
                                                                                parameter,
                                                                                scopeContext,
                                                                                scopeResolverExpression,
                                                                                unresolvedObjectScopeSpecification);

         if (!scopeSpecification.satisfiedBy(resolvedElement)) {
            throw new ParameterScopeException(parameter.name(),
                                              parameter.value(),
                                              String.format(
                                                    "Scope check failed for '%s' value in '%s' parameter of '%s' operation using '%s' scope context",
                                                    element + (!resolvedElement.equals(element)
                                                               ? "->"
                                                                 + resolvedElement
                                                               : ""),
                                                    parameter.name(),
                                                    methodDisplayName,
                                                    scopeContext));
         }
      }));
   }

   /**
    * Computes the specification for the specified stream element, in the context of a parameter stream.
    *
    * @param resolvedElement stream element after resolver evaluation
    * @param parameter stream parameter as passed to method
    * @param scopeContext scope context for parameter
    * @param scopeResolverExpression scope resolver expression for parameter
    * @param unresolvedObjectScopeSpecification reference pointer to the common specification for all
    *       stream elements when no resolver is specified, as an optimization
    *
    * @return resolved stream element specification
    */
   @SuppressWarnings("rawtypes")
   private Specification parameterStreamElementSpecification(Object resolvedElement,
                                                             ParameterModel parameter,
                                                             ScopeContext scopeContext,
                                                             String scopeResolverExpression,
                                                             AtomicReference<Optional<? extends Specification<?>>> unresolvedObjectScopeSpecification) {
      Specification scopeSpecification;
      if (scopeResolverExpression != null) {
         scopeSpecification = scopeService
               .scopeSpecification(resolvedElement.getClass(), scopeContext)
               .orElseThrow(() -> new NotRegisteredScopeException(resolvedElement.getClass()));
      } else {
         Class<?> resolveParameterScopedClass = resolveParameterScopedClass(parameter);

         if (unresolvedObjectScopeSpecification.get() == null) {
            unresolvedObjectScopeSpecification.set(scopeService.scopeSpecification(resolveParameterScopedClass,
                                                                                   scopeContext));
         }

         scopeSpecification = unresolvedObjectScopeSpecification
               .get()
               .orElseThrow(() -> new NotRegisteredScopeException(resolveParameterScopedClass));
      }

      return scopeSpecification;
   }

   /**
    * Updates {@link Stream} return object by instrumenting stream with scope checking of each element at read
    * time.
    *
    * @param returnObject return object value
    * @param methodDisplayName current method display name
    * @param scopeContext scope context for parameter
    */
   @SuppressWarnings("rawtypes")
   private Stream<?> updateStreamReturnObjectWithScope(Stream<?> returnObject,
                                                       Method method,
                                                       String methodDisplayName,
                                                       ScopeContext scopeContext,
                                                       String scopeResolverExpression,
                                                       List<ParameterModel> parameters) {
      if (log.isDebugEnabled()) {
         log.debug(
               "[Scope] Instrument '{}' return object of '{}' operation with scope checking using '{}' scope context",
               returnObjectString(returnObject, returnObject),
               methodDisplayName,
               scopeContext);
      }

      final AtomicReference<Optional<? extends Specification<?>>> unresolvedObjectScopeSpecification =
            new AtomicReference<>();

      return returnObject.peek(element -> {
         Object resolvedElement = resolveScopedObject(element, scopeResolverExpression, parameters, true);

         Specification scopeSpecification = returnObjectStreamElementSpecification(resolvedElement,
                                                                                   method,
                                                                                   scopeContext,
                                                                                   scopeResolverExpression,
                                                                                   unresolvedObjectScopeSpecification);

         if (!scopeSpecification.satisfiedBy(resolvedElement)) {
            throw new ReturnObjectScopeException(returnObject,
                                                 String.format(
                                                       "Scope check failed for '%s' value in return object of '%s' operation using '%s' scope context",
                                                       element + (!resolvedElement.equals(element)
                                                                  ? "->"
                                                                    + resolvedElement
                                                                  : ""),
                                                       methodDisplayName,
                                                       scopeContext));
         }
      });
   }

   /**
    * Computes the specification for the specified stream element, in the context of a returned stream.
    *
    * @param resolvedElement stream element after resolver evaluation
    * @param method method returning the stream
    * @param scopeContext scope context for parameter
    * @param scopeResolverExpression scope resolver expression for parameter
    * @param unresolvedObjectScopeSpecification reference pointer to the common specification for all
    *       stream elements when no resolver is specified, as an optimization
    *
    * @return resolved stream element specification
    */
   @SuppressWarnings("rawtypes")
   private Specification returnObjectStreamElementSpecification(Object resolvedElement,
                                                                Method method,
                                                                ScopeContext scopeContext,
                                                                String scopeResolverExpression,
                                                                AtomicReference<Optional<? extends Specification<?>>> unresolvedObjectScopeSpecification) {
      Specification scopeSpecification;
      if (scopeResolverExpression != null) {
         scopeSpecification = scopeService
               .scopeSpecification(resolvedElement.getClass(), scopeContext)
               .orElseThrow(() -> new NotRegisteredScopeException(resolvedElement.getClass()));
      } else {
         Class<?> resolveReturnObjectScopedClass = resolveMethodScopedClass(method);

         if (unresolvedObjectScopeSpecification.get() == null) {
            unresolvedObjectScopeSpecification.set(scopeService.scopeSpecification(
                  resolveReturnObjectScopedClass,
                  scopeContext));
         }

         scopeSpecification = unresolvedObjectScopeSpecification
               .get()
               .orElseThrow(() -> new NotRegisteredScopeException(resolveReturnObjectScopedClass));
      }

      return scopeSpecification;
   }

   /**
    * Returns parameters values for proceeding, generated from {@link ParameterModel}.
    *
    * @param parameters original list of parameters
    *
    * @return list of parameters for proceeding
    */
   private Object[] proceedingParameters(List<ParameterModel> parameters) {
      return parameters.stream().map(ParameterModel::value).toArray(Object[]::new);
   }

   /**
    * Returns scope ignored expression.
    *
    * @param scoped scoped annotation or {@code null}
    * @param method checked method
    *
    * @return scope ignored expression or {@code null} if no scope ignored configured
    */
   private String scopeIgnoredExpression(Scoped scoped, Method method) {
      return scopedIgnoredAnnotation(scoped, method).map(ScopedIgnored::value).orElse(null);
   }

   /**
    * Resolves {@link ScopedIgnored} annotation.
    *
    * @param scoped scoped annotation or {@code null}
    * @param method checked method
    *
    * @return scope ignored annotation or {@link Optional#empty} if no scope ignored annotation found
    */
   private Optional<ScopedIgnored> scopedIgnoredAnnotation(Scoped scoped, Method method) {
      notNull(method, "method");

      Optional<ScopedIgnored> scopedIgnored =
            nullable(scoped).flatMap(s -> nullable(s.ignore())).filter(i -> i.length > 0).map(i -> i[0]);

      if (!scopedIgnored.isPresent()) {
         scopedIgnored = nullable(findAnnotation(method, ScopedIgnored.class));
      }

      if (!scopedIgnored.isPresent()) {
         scopedIgnored = nullable(findAnnotation(method.getDeclaringClass(), ScopedIgnored.class));
      }

      return scopedIgnored;
   }

   /**
    * Returns scope context expression.
    *
    * @param scoped scoped annotation or {@code null}
    * @param method checked method
    *
    * @return scope context expression or {@code null} if no scope context configured
    */
   private String scopeContextExpression(Scoped scoped, Method method) {
      return scopedContextAnnotation(scoped, method).map(ScopedContext::value).orElse(null);
   }

   /**
    * Resolves {@link ScopedContext} annotation.
    *
    * @param scoped scoped annotation or {@code null}
    * @param method checked method
    *
    * @return scope context annotation or {@link Optional#empty} if no scope context annotation found
    */
   private Optional<ScopedContext> scopedContextAnnotation(Scoped scoped, Method method) {
      notNull(method, "method");

      Optional<ScopedContext> scopedContext =
            nullable(scoped).flatMap(s -> nullable(s.ctx())).filter(ctx -> ctx.length > 0).map(ctx -> ctx[0]);

      if (!scopedContext.isPresent()) {
         scopedContext = nullable(findAnnotation(method, ScopedContext.class));
      }

      if (!scopedContext.isPresent()) {
         scopedContext = nullable(findAnnotation(method.getDeclaringClass(), ScopedContext.class));
      }

      return scopedContext;
   }

   /**
    * Returns scope resolver expression.
    *
    * @param scoped scoped annotation or {@code null}
    * @param method checked method
    *
    * @return scope resolver expression or {@code null} if no scope resolver configured
    */
   private String scopeResolverExpression(Scoped scoped, Method method) {
      return scopedResolverAnnotation(scoped, method).map(ScopedResolver::value).orElse(null);
   }

   /**
    * Resolves {@link ScopedResolver} annotation.
    *
    * @param scoped scoped annotation or {@code null}
    * @param method checked method
    *
    * @return scope resolver annotation or {@link Optional#empty} if no scope resolver annotation found
    */
   private Optional<ScopedResolver> scopedResolverAnnotation(Scoped scoped, Method method) {
      notNull(method, "method");

      Optional<ScopedResolver> scopedResolver = nullable(scoped)
            .flatMap(s -> nullable(s.resolver()))
            .filter(resolver -> resolver.length > 0)
            .map(resolver -> resolver[0]);

      if (!scopedResolver.isPresent()) {
         scopedResolver = nullable(findAnnotation(method, ScopedResolver.class));
      }

      if (!scopedResolver.isPresent()) {
         scopedResolver = nullable(findAnnotation(method.getDeclaringClass(), ScopedResolver.class));
      }

      return scopedResolver;
   }

   /**
    * Resolves scope annotation for the specified parameter.
    * <p>
    * {@link Scoped}/{@link ScopedSet} annotation is searched on parameter, then on parameter class if no
    * annotations are found. If  parameter class is
    * {@link Iterable}/{@link Optional}/{@link Stream}/{@link Specification}, the annotation is searched on
    * generic type class.
    *
    * @param parameter scoped parameter
    *
    * @return scope annotations, never {@code null}
    */
   private List<Scoped> parameterScopedAnnotations(ParameterModel parameter) {
      notNull(parameter, "parameter");

      List<Scoped> scopedSet = list();

      nullable(getAnnotation(parameter.parameter().getParameter(), Scoped.class)).map(scopedSet::add);
      nullable(getAnnotation(parameter.parameter().getParameter(), ScopedSet.class))
            .map(set -> list(set.value()))
            .map(scopedSet::addAll);

      if (scopedSet.isEmpty()) {
         nullable(findAnnotation(resolveParameterScopedClass(parameter), Scoped.class)).map(scopedSet::add);
         nullable(findAnnotation(resolveParameterScopedClass(parameter), ScopedSet.class))
               .map(set -> list(set.value()))
               .map(scopedSet::addAll);
      }

      return scopedSet;
   }

   /**
    * Resolves scope annotation for the specified method.
    * <p>
    * {@link Scoped}/{@link ScopedSet} annotation is searched on method, then on enclosing class if no
    * annotations are found. If return object class is
    * {@link Iterable}/{@link Optional}/{@link Stream}/{@link Specification}, the annotation is searched on
    * generic type class.
    *
    * @param method scoped method
    *
    * @return scope annotations, never {@code null}
    */
   private List<Scoped> methodScopedAnnotation(Method method) {
      notNull(method, "method");

      List<Scoped> scopedSet = list();

      nullable(getAnnotation(method, Scoped.class)).ifPresent(scopedSet::add);
      nullable(getAnnotation(method, ScopedSet.class))
            .map(set -> list(set.value()))
            .ifPresent(scopedSet::addAll);

      if (scopedSet.isEmpty()) {
         nullable(findAnnotation(resolveMethodScopedClass(method), Scoped.class)).ifPresent(scopedSet::add);
         nullable(findAnnotation(resolveMethodScopedClass(method), ScopedSet.class))
               .map(set -> list(set.value()))
               .ifPresent(scopedSet::addAll);
      }

      return scopedSet;
   }

   /**
    * Returns method parameters.
    *
    * @param joinPoint join point
    *
    * @return method's parameters
    */
   private static List<ParameterModel> parameters(JoinPoint joinPoint) {
      List<ParameterModel> parameters = new ArrayList<>();
      Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();

      for (int pi = 0; pi < method.getParameterCount(); pi++) {
         Object parameterObject = joinPoint.getArgs()[pi];

         parameters.add(new ParameterModel(new MethodParameter(method, pi), parameterObject));
      }

      return parameters;
   }

   /**
    * Resolves class of scoped object with support for generics from parameter.
    *
    * @param parameter parameter to resolve scoped class
    *
    * @return scoped object class
    */
   private Class<?> resolveParameterScopedClass(ParameterModel parameter) {
      return resolveScopedClass(ResolvableType.forMethodParameter(parameter.parameter())).resolve();
   }

   /**
    * Resolves class of scoped object with support for generics from method.
    *
    * @param method method to resolve return type scoped class
    *
    * @return scoped object class
    */
   private Class<?> resolveMethodScopedClass(Method method) {
      return resolveScopedClass(ResolvableType.forMethodReturnType(method)).resolve();
   }

   /**
    * Resolves specified type recursively following declared generics.
    * Supported genericized types :
    * <ul>
    *    <li>{@link Optional} or subtypes</li>
    *    <li>{@link Stream} or subtypes</li>
    *    <li>{@link Iterable} or subtypes</li>
    *    <li>{@link Specification} or subtypes</li>
    * </ul>
    * An arbitrary composition of the supported genericized types is also supported, or any class, genericized or not, implementing one of these.
    *
    * @param type type to resolve
    *
    * @return final resolvable type
    */
   private ResolvableType resolveScopedClass(ResolvableType type) {
      if (OPTIONAL_TYPE.isAssignableFrom(type)) {
         return resolveScopedClass(type.as(Optional.class).getGeneric(0));
      } else if (ITERABLE_TYPE.isAssignableFrom(type)) {
         return resolveScopedClass(type.as(Iterable.class).getGeneric(0));
      } else if (STREAM_TYPE.isAssignableFrom(type)) {
         return resolveScopedClass(type.as(Stream.class).getGeneric(0));
      } else if (SPECIFICATION_TYPE.isAssignableFrom(type)) {
         return resolveScopedClass(type.as(Specification.class).getGeneric(0));
      } else {
         return type;
      }
   }

   /**
    * Evaluates SpEL expression.
    *
    * @param parameters method parameters
    * @param expression SpEL expression, optionally {@code null}/blank.
    * @param rootObject optional root object
    * @param <T> evaluation result type
    *
    * @return evaluation result or {@link Optional#empty()} if expression is {@code null}/blank or
    *       evaluation result is {@code null}
    */
   @SuppressWarnings("unchecked")
   private <T> Optional<T> evaluateExpression(List<ParameterModel> parameters,
                                              String expression,
                                              Object rootObject) {
      notNull(parameters, "parameters");

      if (StringUtils.isBlank(expression)) {
         return optional();
      }

      Expression parsedExpression = expressionCache.computeIfAbsent(expression, s -> {
         ExpressionParser expressionParser = new SpelExpressionParser();
         return expressionParser.parseExpression(expression);
      });

      StandardEvaluationContext context = new StandardEvaluationContext();
      context.setBeanResolver(new BeanFactoryResolver(ctx));
      context.setRootObject(rootObject);

      registerConditionFunction(context, "hasProperty", String.class, String.class, boolean.class);
      registerConditionFunction(context, "hasProperty", String.class, String.class);
      registerConditionFunction(context, "hasEnvironment", String.class, String.class, boolean.class);
      registerConditionFunction(context, "hasEnvironment", String.class, String.class);
      registerConditionFunction(context, "hasSpringEnvironment", String.class, String.class, boolean.class);
      registerConditionFunction(context, "hasSpringEnvironment", String.class, String.class);
      registerConditionFunction(context, "hasSpringProfile", String.class);

      Map<String, Object> variables = new HashMap<>();
      for (ParameterModel parameter : parameters) {
         variables.put(parameter.name(), parameter.value());
      }
      context.setVariables(variables);

      return nullable((T) parsedExpression.getValue(context));
   }

   private void registerConditionFunction(StandardEvaluationContext context,
                                          String name,
                                          Class<?>... parameterTypes) {
      try {
         context.registerFunction(name, ConditionFunctions.class.getMethod(name, parameterTypes));
      } catch (NoSuchMethodException e) {
         throw new IllegalStateException(e);
      }
   }

   /**
    * Resolves original method instead of proxied one.
    *
    * @param joinPoint AOP join point
    *
    * @return original method
    */
   private Method resolveUserMethod(JoinPoint joinPoint) {
      Method proxiedMethod = ((MethodSignature) joinPoint.getSignature()).getMethod();

      try {
         return resolveUserClass(joinPoint).getMethod(proxiedMethod.getName(),
                                                      proxiedMethod.getParameterTypes());
      } catch (NoSuchMethodException e) {
         throw new IllegalStateException(e);
      }
   }

   /**
    * Resolves original class instead of proxied one.
    *
    * @param joinPoint AOP join point
    *
    * @return original class
    */
   private Class<?> resolveUserClass(JoinPoint joinPoint) {
      return ClassUtils.getUserClass(joinPoint.getThis());
   }

   /**
    * Internal parameter model including parameter value.
    */
   private static class ParameterModel {
      private final MethodParameter parameter;
      private final String name;
      private final Class<?> type;
      private Object value;

      public ParameterModel(MethodParameter parameter, Object value) {
         this.parameter = notNull(parameter, "parameter");
         this.name = parameter.getParameter().getName();
         this.type = parameter.getParameterType();
         this.value = value;
      }

      public MethodParameter parameter() {
         return parameter;
      }

      public String name() {
         return name;
      }

      public Class<?> type() {
         return type;
      }

      /**
       * Returns parameter value.
       *
       * @return parameter value or {@code null} if parameter value is {@code null}
       */
      public Object value() {
         return value;
      }

      public void updateValue(Object value) {
         satisfies(value,
                   v -> type.isAssignableFrom(v.getClass()),
                   "value",
                   String.format("must be an instance of '%s'", type));

         this.value = value;
      }
   }

   /**
    * SpEL root object model to provide scoped object to SpEL expression {@link #evaluateExpression(List,
    * String, Object)}.
    */
   private static class ScopedRoot {
      private final Object scopedObject;
      private final boolean isReturnObject;

      public ScopedRoot(Object scopedObject, boolean isReturnObject) {
         this.scopedObject = scopedObject;
         this.isReturnObject = isReturnObject;
      }

      public Object getScopedObject() {
         return scopedObject;
      }

      public boolean isReturnObject() {
         return isReturnObject;
      }
   }

   /** Holds a default {@link Scoped} instance. */
   @Scoped
   private static class DefaultScopedHolder {
      private static Scoped defaultScoped() {
         return DefaultScopedHolder.class.getAnnotation(Scoped.class);
      }
   }

}