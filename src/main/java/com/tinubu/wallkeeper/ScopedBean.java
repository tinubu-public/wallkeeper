/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a class so that all its public methods return values will be checked for scope.
 * You can exclude a method from check using {@link Scoped} with {@code ignore=true} on method.
 * <p>
 * Generally, parameters must be explicitly annotated with {@link Scoped} (or {@link ScopedSet}) in method
 * signature to be checked.
 * If a class is annotated with {@link ScopedBean}, parameters which class is annotated or meta-annotated
 * with {@link Scoped} will be checked, even if parameter is not annotated in method signature. You can
 * exclude such a parameter from check using {@link Scoped} with {@code ignore=true} on parameter in method
 * signature.
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ScopedBean {}
