/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.tinubu.commons.ddd2.domain.specification.Specification;

/**
 * Defines a global scope registry for all scopable objects.
 * <p>
 * You have to declare manually this bean into the Spring context, from a
 * {@link Configuration}, and register scope factories for scopable objects using {@link #registerScope(Class,
 * ScopeSpecificationFactory)}.
 * <p>
 * You can register a custom {@link ScopeSpecificationFactory} or a {@link CachingScopeSpecificationFactory}
 * for example.
 * <p>
 * Scope registration and access is thread-safe.
 */
@Component("wallkeeper.scopeFactory")
public class ScopeFactory<SC extends ScopeContext, S extends UserScope> {

   /** Java primitive classes to register in {@link #registerDefaultScopeForPrimitives()}. */
   private static final List<Class<?>> PRIMITIVE_CLASSES = Arrays.asList(Boolean.class,
                                                                         Byte.class,
                                                                         Character.class,
                                                                         Short.class,
                                                                         Integer.class,
                                                                         Long.class,
                                                                         Float.class,
                                                                         Double.class);

   private final Map<Class<?>, ScopeSpecificationFactory<?, SC, S>> scopeFactoryRegistry =
         new ConcurrentHashMap<>();

   /**
    * Registers a scope specification factory for the specified object class.
    *
    * @param objectClass object class managed by this registered factory
    * @param scopeFactory scope specification factory
    * @param <T> object type
    */
   public <T> void registerScope(Class<T> objectClass, ScopeSpecificationFactory<T, SC, S> scopeFactory) {
      notNull(objectClass, "objectClass");
      notNull(scopeFactory, "specification");

      scopeFactoryRegistry.put(objectClass, scopeFactory);
   }

   /**
    * Registers the default scope specification factory for the specified object class.
    *
    * @param objectClass object class managed by this registered factory
    * @param <T> object type
    */
   public <T> void registerDefaultScope(Class<T> objectClass) {
      registerScope(objectClass, defaultScopeFactory());
   }

   /**
    * Registers the default scope specification factory for a set of Java primitive types.
    *
    * @param <T> object type
    */
   public <T> void registerDefaultScopeForPrimitives() {
      for (Class<?> primitiveClass : PRIMITIVE_CLASSES) {
         registerScope(primitiveClass, defaultScopeFactory());
      }
   }

   /**
    * Generates a default scope factory that generate an always satisfied scope specification.
    *
    * @param <T> object type
    *
    * @return a scope factory that generate an always satisfied scope specification
    */
   public <T> ScopeSpecificationFactory<T, SC, S> defaultScopeFactory() {
      return (context, userScope) -> __ -> true;
   }

   /**
    * Generates a scope specification for specified object class, context and authentication scope.
    * Returns specification registered for specified object class, or if not found, any of its supertypes.
    * Returned specification is non-deterministic if multiple supertypes of the specified object class are
    * registered in the {@link ScopeFactory}.
    *
    * @param objectClass object class to generate scope for
    * @param context optional scope context, or {@code null} if there's no context
    * @param userScope user scope to match
    * @param <T> object type
    *
    * @return new scope specification or {@link Optional#empty} if no scope defined for specified class
    */
   @SuppressWarnings({ "OptionalUsedAsFieldOrParameterType", "unchecked" })
   public <T> Optional<Specification<T>> scope(Class<T> objectClass, SC context, Optional<S> userScope) {
      notNull(objectClass, "objectClass");
      notNull(userScope, "userScope");

      Optional<ScopeSpecificationFactory<T, SC, S>> scopeSpecificationFactory =
            nullable((ScopeSpecificationFactory<T, SC, S>) scopeFactoryRegistry.get(objectClass));

      if (!scopeSpecificationFactory.isPresent()) {
         scopeSpecificationFactory = scopeFactoryRegistry
               .entrySet()
               .stream()
               .filter(factory -> factory.getKey().isAssignableFrom(objectClass))
               .map(factory -> (ScopeSpecificationFactory<T, SC, S>) factory.getValue())
               .findAny();
      }

      return scopeSpecificationFactory.map(factory -> factory.scopeSpecification(context, userScope));
   }
}
