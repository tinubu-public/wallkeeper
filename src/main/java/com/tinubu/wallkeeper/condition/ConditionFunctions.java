/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.condition;

import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Arrays;

import com.tinubu.wallkeeper.autoconfigure.ScopeAutoConfiguration;

/**
 * Registry of condition functions.
 */
public final class ConditionFunctions {

   private ConditionFunctions() {}

   /**
    * Checks if property matches specified Regex.
    *
    * @param named property name
    * @param matches regex
    * @param matchIfMissing whether to success if property is missing
    *
    * @return {@code true} if property check is successful
    */
   public static boolean hasProperty(String named, String matches, boolean matchIfMissing) {
      notBlank(named, "named");
      notNull(matches, "matches");

      String propertyValue = System.getProperty(named);

      if (propertyValue == null) {
         return matchIfMissing;
      } else {
         return propertyValue.matches(matches);
      }
   }

   /**
    * Checks if property matches specified Regex.
    *
    * @param named property name
    * @param matches regex
    *
    * @return {@code true} if property check is successful
    */
   public static boolean hasProperty(String named, String matches) {
      return hasProperty(named, matches, false);
   }

   /**
    * Checks if environment variable matches specified Regex.
    *
    * @param named environment variable name
    * @param matches regex
    * @param matchIfMissing whether to success if variable is missing
    *
    * @return {@code true} if environment variable check is successful
    */
   public static boolean hasEnvironment(String named, String matches, boolean matchIfMissing) {
      notBlank(named, "named");
      notNull(matches, "matches");

      String propertyValue = System.getenv(named);

      if (propertyValue == null) {
         return matchIfMissing;
      } else {
         return propertyValue.matches(matches);
      }
   }

   /**
    * Checks if environment variable matches specified Regex.
    *
    * @param named environment variable name
    * @param matches regex
    *
    * @return {@code true} if environment variable check is successful
    */
   public static boolean hasEnvironment(String named, String matches) {
      return hasEnvironment(named, matches, false);
   }

   /**
    * Checks if Spring environment property matches specified Regex.
    *
    * @param named Spring environment property name
    * @param matches regex
    * @param matchIfMissing whether to success if variable is missing
    *
    * @return {@code true} if Spring environment property check is successful
    */
   public static boolean hasSpringEnvironment(String named, String matches, boolean matchIfMissing) {
      notBlank(named, "named");
      notNull(matches, "matches");

      String propertyValue = ScopeAutoConfiguration.environment().getProperty(named);

      if (propertyValue == null) {
         return matchIfMissing;
      } else {
         return propertyValue.matches(matches);
      }
   }

   /**
    * Checks if Spring environment property matches specified Regex.
    *
    * @param named Spring environment property name
    * @param matches regex
    *
    * @return {@code true} if Spring environment property check is successful
    */
   public static boolean hasSpringEnvironment(String named, String matches) {
      return hasSpringEnvironment(named, matches, false);
   }

   /**
    * Checks if Spring profile is active.
    *
    * @param named Spring profile name
    *
    * @return {@code true} if Spring profile check is successful
    */
   public static boolean hasSpringProfile(String named) {
      notBlank(named, "named");

      return Arrays.asList(ScopeAutoConfiguration.environment().getActiveProfiles()).contains(named);
   }

}
