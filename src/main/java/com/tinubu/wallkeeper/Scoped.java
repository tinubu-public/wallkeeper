/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Explicitly marks an element to be checked for scope :
 * <ul>
 *    <li>a method/method return type to check the returned value</li>
 *    <li>a method parameter to check the parameter</li>
 *    <li>a class that will be checked if used as a method parameter or return value</li>
 * </ul>
 * <p>
 * If this annotation is repeated on an element, all scopes will have to be satisfied to access the element.
 */
@Target({ ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(ScopedSet.class)
public @interface Scoped {

   /**
    * Ignore scope configuration.
    *
    * @return ignore scope configuration
    */
   ScopedIgnored[] ignore() default {};

   /**
    * Scope context configuration
    *
    * @return scope context configuration
    */
   ScopedContext[] ctx() default {};

   /**
    * Scope resolver configuration.
    *
    * @return scope resolver configuration
    */
   ScopedResolver[] resolver() default {};
}
