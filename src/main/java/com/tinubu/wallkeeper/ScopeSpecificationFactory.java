/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper;

import java.util.Optional;

import com.tinubu.commons.ddd2.domain.specification.Specification;

/**
 * Defines a scope specification factory. Generates a specification of the specified {@link T} type satisfying
 * objects in the specified scope.
 *
 * @param <T> scoped object type
 * @param <SC> scope context type
 * @param <S> user scope type
 */
@FunctionalInterface
public interface ScopeSpecificationFactory<T, SC extends ScopeContext, S extends UserScope> {

   /**
    * Generates a specification of the specified {@link T} type satisfying objects in the specified scope.
    *
    * @param context optional scope context, or {@code null} if there's no context. You can generate
    *       different scope specifications depending on different contexts
    * @param userScope user scope or {@code Optional#empty} if user has no scope (i.e. unscoped users)
    *
    * @return scope specification
    */
   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   Specification<T> scopeSpecification(SC context, Optional<S> userScope);

   /**
    * Generates a specification of the specified {@link T} type satisfying objects in the specified scope,
    * with {@code null} default context.
    *
    * @param userScope user scope or {@code Optional#empty} if there's no scope (i.e. unscoped users)
    *
    * @return scope specification
    */
   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   default Specification<T> scopeSpecification(Optional<S> userScope) {
      return scopeSpecification(null, userScope);
   }
}
