/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import com.tinubu.commons.ddd2.domain.specification.CompositeSpecification;
import com.tinubu.commons.ddd2.domain.specification.Specification;

@Component
public class ScopeService<SC extends ScopeContext, S extends UserScope> {

   private final UserScopeProvider<S> userScopeProvider;
   private final ScopeFactory<SC, S> scopeFactory;

   public ScopeService(UserScopeProvider<S> userScopeProvider, ScopeFactory<SC, S> scopeFactory) {
      this.userScopeProvider = userScopeProvider;
      this.scopeFactory = scopeFactory;
   }

   /**
    * Returns user scope or {@link Optional#empty} if user has no scope.
    *
    * @return user scope or {@link Optional#empty} if user has no scope
    */
   public Optional<S> userScope() {
      return notNull(userScopeProvider.userScope(), "userScope");
   }

   /**
    * Returns registered scope specification for specified context and object class.
    *
    * @param <T> object type
    * @param objectClass object class
    * @param context optional scope context, or {@code null} if there's no context
    *
    * @return scope specification or {@link Optional#empty} if no scope registered for specified object
    */
   public <T> Optional<Specification<T>> scopeSpecification(Class<T> objectClass, SC context) {
      notNull(objectClass, "objectClass");

      return scopeFactory.scope(objectClass, context, userScope());
   }

   /**
    * Returns registered scope specification for specified context and object.
    *
    * @param <T> object type
    * @param object object
    * @param context optional scope context, or {@code null} if there's no context
    *
    * @return scope specification or {@link Optional#empty} if no scope registered for specified object
    */
   @SuppressWarnings("unchecked")
   public <T> Optional<Specification<T>> scopeSpecification(T object, SC context) {
      return scopeSpecification((Class<T>) notNull(object, "object").getClass(), context);
   }

   /**
    * Returns specified composite specification filtered with registered scope for specified object class.
    * If the specified object is not scopable, returns existing specification.
    *
    * @param <T> object type
    * @param specification existing specification
    * @param objectClass object class
    * @param context optional scope context, or {@code null} if there's no context
    *
    * @return scoped composite specification
    */
   public <T> CompositeSpecification<T> scopedSpecification(CompositeSpecification<T> specification,
                                                            Class<T> objectClass,
                                                            SC context) {
      notNull(specification, "specification");
      notNull(objectClass, "objectClass");

      return scopeSpecification(objectClass, context).map(specification::and).orElse(specification);
   }

   /**
    * Returns specified composite specification filtered with registered scope for specified object class.
    * If the specified object is not scopable, returns existing specification.
    *
    * @param <T> object type
    * @param specification existing specification
    * @param objectClass object class
    * @param context optional scope context, or {@code null} if there's no context
    *
    * @return scoped specification
    */
   public <T> CompositeSpecification<T> scopedSpecification(Specification<T> specification,
                                                            Class<T> objectClass,
                                                            SC context) {
      notNull(specification, "specification");
      notNull(objectClass, "objectClass");

      if (specification instanceof CompositeSpecification) {
         return scopedSpecification((CompositeSpecification<T>) specification, objectClass, context);
      } else {
         return scopeSpecification(objectClass, context)
               .map(scopedSpecification -> CompositeSpecification.of(specification).and(scopedSpecification))
               .orElse(CompositeSpecification.of(specification));
      }
   }

   /**
    * Returns {@code true} if specified object is scopable and is in the authenticated user scope.
    * An object is scopable if its type is registered in {@link ScopeFactory}.
    * A {@code null} object is considered in scope.
    * Container objects are supported :
    * <ul>
    *    <li>{@link Optional}</li>
    *    <li>{@link Iterable}</li>
    *    <li>{@link Stream}</li>
    * </ul>
    *
    * @param <T> object type
    * @param object object to check, or {@code null} if object value is {@code null}
    * @param context optional scope context, or {@code null} if there's no context
    * @param requiredScope if {@code true}, operations fails if no scope defined for specified object
    *
    * @return {@code true} if object is correctly scoped
    *
    * @throws NotRegisteredScopeException if no scope defined for specified object and scope is required
    */
   public <T> boolean inScope(T object, SC context, boolean requiredScope) {
      if (object == null) {
         return true;
      }

      if (object instanceof Optional) {
         return inScope((Optional<?>) object, context, requiredScope);
      } else if (object instanceof Iterable) {
         return inScope((Iterable<?>) object, context, requiredScope);
      } else {
         Optional<Specification<T>> scopeSpecification = scopeSpecification(object, context);

         if (!scopeSpecification.isPresent() && requiredScope) {
            throw new NotRegisteredScopeException(object.getClass());
         }

         return scopeSpecification.map(s -> s.satisfiedBy(object)).orElse(true);
      }
   }

   /**
    * Returns {@code true} if optional object is scopable and is in the authenticated user scope.
    * An object is scopable if its type is registered in {@link ScopeFactory}.
    * A {@link Optional#empty} or {@code null} object is considered in scope.
    *
    * @param <T> object type
    * @param object optional object to check, or {@code null} is object is {@code null}
    * @param context optional scope context, or {@code null} if there's no context
    * @param requiredScope if {@code true}, operations fails if no scope defined for specified object
    *
    * @return {@code true} if all page objects are correctly scoped
    *
    * @throws NotRegisteredScopeException if no scope defined for specified object and scope is required
    */
   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   public <T> boolean inScope(Optional<T> object, SC context, boolean requiredScope) {
      if (object == null || !object.isPresent()) {
         return true;
      }

      return inScope(object.get(), context, requiredScope);
   }

   /**
    * Returns {@code true} if each specified object is scopable and is in the authenticated user scope.
    * An object is scopable if its type is registered in {@link ScopeFactory}.
    * A {@code null} iterable, or {@code null} element in iterable, is considered in scope.
    *
    * @param <T> object type
    * @param objects objects to check, or {@code null} if iterable object is {@code null}
    * @param context optional scope context, or {@code null} if there's no context
    * @param requiredScope if {@code true}, operations fails if no scope defined for specified object
    *
    * @return {@code true} if all objects are correctly scoped
    *
    * @throws NotRegisteredScopeException if no scope defined for specified object and scope is required
    */
   public <T> boolean inScope(Iterable<T> objects, SC context, boolean requiredScope) {
      if (objects == null) {
         return true;
      }

      for (T object : objects) {
         if (!inScope(object, context, requiredScope)) {
            return false;
         }
      }

      return true;
   }

}
