package com.tinubu.wallkeeper.autoconfigure;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;

import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;

import com.tinubu.wallkeeper.ScopeAspect;
import com.tinubu.wallkeeper.ScopeFactory;
import com.tinubu.wallkeeper.ScopeService;
import com.tinubu.wallkeeper.UserScope;
import com.tinubu.wallkeeper.UserScopeProvider;
import com.tinubu.wallkeeper.condition.ConditionFunctions;

@Configuration
@Import({ ScopeService.class, ScopeFactory.class, ScopeAspect.class })
public class ScopeAutoConfiguration implements EnvironmentAware {

   private static Environment environment;

   /**
    * Provides a default {@link UserScopeProvider} which returns a no use {@link UserScope}.
    * You should override this bean in each application to provide your user scope definition.
    *
    * @return default scope provider
    */
   @Bean("wallkeeper.userScopeProvider")
   public UserScopeProvider<UserScope> userScopeProvider() {
      return () -> optional(new UserScope() {});
   }

   @Override
   public void setEnvironment(Environment environment) {
      ScopeAutoConfiguration.environment = environment;
   }

   /**
    * References Spring environment for access from {@link ConditionFunctions} functions.
    */
   public static Environment environment() {
      return environment;
   }
}