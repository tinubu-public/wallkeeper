/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper;

import org.springframework.security.access.AccessDeniedException;

public class ParameterScopeException extends AccessDeniedException {

   private final String parameterName;
   private final Object parameterObject;

   public ParameterScopeException(String parameterName, Object parameterObject, String msg) {
      super(msg);
      this.parameterName = parameterName;
      this.parameterObject = parameterObject;
   }

   public ParameterScopeException(String parameterName, Object parameterObject, String msg, Throwable t) {
      super(msg, t);
      this.parameterName = parameterName;
      this.parameterObject = parameterObject;
   }

   public String parameterName() {
      return parameterName;
   }

   public Object parameterObject() {
      return parameterObject;
   }
}
