/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.trigger;

import org.springframework.stereotype.Component;

import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.ScopedContext;
import com.tinubu.wallkeeper.ScopedIgnored;
import com.tinubu.wallkeeper.ScopedSet;
import com.tinubu.wallkeeper.type.FailSensitive;
import com.tinubu.wallkeeper.type.Registered;
import com.tinubu.wallkeeper.type.Sensitive;

@Component
@ScopedBean
public class ScopedSetAnnotatedBean {

   @Scoped
   public Registered singledScopedMethod() {
      return new Registered();
   }

   @Scoped
   public Registered singledScopedMethodWithParameter(@Scoped Registered parameter) {
      return new Registered();
   }

   @Scoped
   @Scoped
   public Registered multipleScopedMethod() {
      return new Registered();
   }

   @Scoped
   @Scoped
   public Registered multipleScopedMethodWithParameter(@Scoped @Scoped Registered parameter) {
      return new Registered();
   }

   @ScopedSet({ @Scoped, @Scoped })
   public Registered scopedSetMethod() {
      return new Registered();
   }

   @ScopedSet({ @Scoped, @Scoped })
   public Registered scopedSetMethodWithParameter(@ScopedSet({ @Scoped, @Scoped }) Registered parameter) {
      return new Registered();
   }

   @Scoped(ignore = @ScopedIgnored)
   @Scoped(ignore = @ScopedIgnored)
   public Registered multipleIgnoredScopedMethod() {
      return new Registered();
   }

   @Scoped(ignore = @ScopedIgnored)
   @Scoped(ignore = @ScopedIgnored)
   public Registered multipleIgnoredScopedMethodWithParameter(
         @Scoped(ignore = @ScopedIgnored) @Scoped(ignore = @ScopedIgnored) Registered parameter) {
      return new Registered();
   }

   @Scoped
   @Scoped(ctx = @ScopedContext("@scopeContext.fail()"))
   public Registered partialFailureScopedMethod() {
      return new Registered();
   }

   @Scoped
   @Scoped(ctx = @ScopedContext("@scopeContext.fail()"))
   public Registered partialFailureScopedMethodWithParameter(
         @Scoped @Scoped(ctx = @ScopedContext("@scopeContext.fail()")) Registered parameter) {
      return new Registered();
   }

   @Scoped
   @Scoped(ctx = @ScopedContext("@scopeContext.fail()"))
   public Sensitive inheritingScopedMethod() {
      return new Sensitive();
   }

   @Scoped
   @Scoped(ctx = @ScopedContext("@scopeContext.fail()"))
   public Sensitive inheritingScopedMethodWithParameter(
         @Scoped @Scoped(ctx = @ScopedContext("@scopeContext.fail()")) Sensitive parameter) {
      return new Sensitive();
   }

   @Scoped
   @Scoped
   public FailSensitive inheritingFailScopedMethod() {
      return new FailSensitive();
   }

   @Scoped
   @Scoped
   public FailSensitive inheritingFailScopedMethodWithParameter(@Scoped @Scoped FailSensitive parameter) {
      return new FailSensitive();
   }

}
