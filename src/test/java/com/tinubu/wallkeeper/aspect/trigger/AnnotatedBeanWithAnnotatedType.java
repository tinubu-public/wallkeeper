/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.trigger;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static java.util.stream.Collectors.toList;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.ScopedIgnored;
import com.tinubu.wallkeeper.type.AnnotatedSuperTypeSensitive;
import com.tinubu.wallkeeper.type.MetaAnnotatedSensitive;
import com.tinubu.wallkeeper.type.NonSensitive;
import com.tinubu.wallkeeper.type.Sensitive;

@Component
@ScopedBean
public class AnnotatedBeanWithAnnotatedType {

   public Sensitive sensitiveReturningMethod() {
      return new Sensitive();
   }

   public NonSensitive nonSensitiveReturningMethod() {
      return new NonSensitive();
   }

   public void methodWithSensitiveParameter(Sensitive parameter) {}

   public void methodWithAnnotatedSuperTypeSensitiveParameter(AnnotatedSuperTypeSensitive parameter) {}

   public void methodWithMetaAnnotatedSensitiveParameter(MetaAnnotatedSensitive parameter) {}

   public void methodWithNonSensitiveParameter(NonSensitive parameter) {}

   @Scoped(ignore = @ScopedIgnored)
   public Sensitive ignoredSensitiveReturningMethod() {
      return new Sensitive();
   }

   public void methodWithIgnoredSensitiveParameter(@Scoped(ignore = @ScopedIgnored) Sensitive parameter) {}

   @Scoped
   public NonSensitive annotatedNonSensitiveReturningMethod() {
      return new NonSensitive();
   }

   public void methodWithAnnotatedNonSensitiveParameter(@Scoped NonSensitive parameter) {}

   public Optional<Sensitive> optionalSensitiveReturningMethod() {
      return optional(new Sensitive());
   }

   public void methodWithOptionalSensitiveParameter(Optional<Sensitive> parameter) {}

   public List<Sensitive> iterableSensitiveReturningMethod() {
      return Arrays.asList(new Sensitive());
   }

   public void methodWithIterableSensitiveParameter(List<Sensitive> parameter) {}

   public MyIterable<String, Sensitive> myIterableSensitiveReturningMethod() {
      return new MyIterable<>(new Sensitive());
   }

   public Stream<Sensitive> streamSensitiveReturningMethod() {
      return Stream.of(new Sensitive());
   }

   public void methodWithStreamSensitiveParameter(Stream<Sensitive> parameter) {
      parameter.collect(toList()); // effectively read the stream.
   }

   public void methodWithMyIterableSensitiveParameter(MyIterable<String, Sensitive> parameter) {}

   public MySensitiveIterable mySensitiveIterableReturningMethod() {
      return new MySensitiveIterable(new Sensitive());
   }

   public void methodWithMySensitiveIterableParameter(MySensitiveIterable parameter) {
   }

   public Optional<List<MyIterable<String, MySensitiveIterable>>> composedSensitiveReturningMethod() {
      return optional(Arrays.asList(new MyIterable<>(new MySensitiveIterable(new Sensitive()))));
   }

   public void methodWithComposedSensitiveParameter(Optional<List<MyIterable<String, MySensitiveIterable>>> parameter) {}

   public <T extends Sensitive> void methodWithOptionalGenericSensitiveParameter(Optional<T> parameter) {}

   public <T extends Optional<Sensitive>> void methodWithGenericOptionalSensitiveParameter(T parameter) {}

   /** Custom {@link Iterable} implementation. T param is not in first position for testing purpose. */
   public static class MyIterable<ANY, T> extends AbstractList<T> {

      private List<T> internalList = new ArrayList<>();

      public MyIterable(T... elements) {
         this.internalList.addAll(Arrays.asList(elements));
      }

      @Override
      public T get(int index) {
         return internalList.get(index);
      }

      @Override
      public int size() {
         return internalList.size();
      }

      @Override
      public T set(int index, T element) {
         return internalList.set(index, element);
      }
   }

   /** Custom {@link Iterable} implementation. */
   public static class MySensitiveIterable extends AbstractList<Sensitive> {

      private List<Sensitive> internalList = new ArrayList<>();

      public MySensitiveIterable(Sensitive... elements) {
         this.internalList.addAll(Arrays.asList(elements));
      }

      @Override
      public Sensitive get(int index) {
         return internalList.get(index);
      }

      @Override
      public int size() {
         return internalList.size();
      }

      @Override
      public Sensitive set(int index, Sensitive element) {
         return internalList.set(index, element);
      }
   }
}
