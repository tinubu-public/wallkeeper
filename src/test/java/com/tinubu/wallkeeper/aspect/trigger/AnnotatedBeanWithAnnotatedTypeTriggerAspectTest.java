/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.trigger;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.Arrays;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.tinubu.wallkeeper.ParameterScopeException;
import com.tinubu.wallkeeper.ReturnObjectScopeException;
import com.tinubu.wallkeeper.aspect.AbstractScopeAspectTest;
import com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType.MyIterable;
import com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType.MySensitiveIterable;
import com.tinubu.wallkeeper.type.AnnotatedSuperTypeSensitive;
import com.tinubu.wallkeeper.type.MetaAnnotatedSensitive;
import com.tinubu.wallkeeper.type.NonSensitive;
import com.tinubu.wallkeeper.type.Sensitive;

@ContextConfiguration(classes = AnnotatedBeanWithAnnotatedTypeTriggerAspectTest.class)
public class AnnotatedBeanWithAnnotatedTypeTriggerAspectTest extends AbstractScopeAspectTest {

   @Autowired
   AnnotatedBeanWithAnnotatedType bean;

   @Test
   public void testSensitiveReturningMethod() {
      setScopeResult(true);

      bean.sensitiveReturningMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.sensitiveReturningMethod())
            .withMessage(
                  "Scope check failed for 'Sensitive[]' return object of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::sensitiveReturningMethod' operation using 'null' scope context");
   }

   @Test
   public void testNonSensitiveReturningMethod() {
      setScopeResult(true);

      bean.nonSensitiveReturningMethod();

      setScopeResult(false);

      bean.nonSensitiveReturningMethod();
   }

   @Test
   public void testMethodWithSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithSensitiveParameter(new Sensitive());

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithSensitiveParameter(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::methodWithSensitiveParameter' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithAnnotatedSuperTypeSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithAnnotatedSuperTypeSensitiveParameter(new AnnotatedSuperTypeSensitive());

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithAnnotatedSuperTypeSensitiveParameter(new AnnotatedSuperTypeSensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=AnnotatedSuperTypeSensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::methodWithAnnotatedSuperTypeSensitiveParameter' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithMetaAnnotatedSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithMetaAnnotatedSensitiveParameter(new MetaAnnotatedSensitive());

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithMetaAnnotatedSensitiveParameter(new MetaAnnotatedSensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=MetaAnnotatedSensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::methodWithMetaAnnotatedSensitiveParameter' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithNonSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithNonSensitiveParameter(new NonSensitive());

      setScopeResult(false);

      bean.methodWithNonSensitiveParameter(new NonSensitive());
   }

   @Test
   public void testIgnoredSensitiveReturningMethod() {
      setScopeResult(true);

      bean.ignoredSensitiveReturningMethod();

      setScopeResult(false);

      bean.ignoredSensitiveReturningMethod();
   }

   @Test
   public void testMethodWithIgnoredSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithIgnoredSensitiveParameter(new Sensitive());

      setScopeResult(false);

      bean.methodWithIgnoredSensitiveParameter(new Sensitive());
   }

   @Test
   public void testAnnotatedNonSensitiveReturningMethod() {
      setScopeResult(true);

      bean.annotatedNonSensitiveReturningMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.annotatedNonSensitiveReturningMethod())
            .withMessage(
                  "Scope check failed for 'NonSensitive[]' return object of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::annotatedNonSensitiveReturningMethod' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithAnnotatedNonSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithAnnotatedNonSensitiveParameter(new NonSensitive());

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithAnnotatedNonSensitiveParameter(new NonSensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=NonSensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::methodWithAnnotatedNonSensitiveParameter' operation using 'null' scope context");
   }

   @Test
   public void testOptionalSensitiveReturningMethod() {
      setScopeResult(true);

      bean.optionalSensitiveReturningMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.optionalSensitiveReturningMethod())
            .withMessage(
                  "Scope check failed for 'Optional[Sensitive[]]' return object of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::optionalSensitiveReturningMethod' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithOptionalSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithOptionalSensitiveParameter(optional(new Sensitive()));

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithOptionalSensitiveParameter(optional(new Sensitive())))
            .withMessage(
                  "Scope check failed for 'parameter=Optional[Sensitive[]]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::methodWithOptionalSensitiveParameter' operation using 'null' scope context");
   }

   @Test
   public void testIterableSensitiveReturningMethod() {
      setScopeResult(true);

      bean.iterableSensitiveReturningMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.iterableSensitiveReturningMethod())
            .withMessage(
                  "Scope check failed for '[Sensitive[]]' return object of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::iterableSensitiveReturningMethod' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithIterableSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithIterableSensitiveParameter(Arrays.asList(new Sensitive()));

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithIterableSensitiveParameter(Arrays.asList(new Sensitive())))
            .withMessage(
                  "Scope check failed for 'parameter=[Sensitive[]]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::methodWithIterableSensitiveParameter' operation using 'null' scope context");
   }

   @Test
   public void testStreamSensitiveReturningMethod() {
      setScopeResult(true);

      bean.streamSensitiveReturningMethod();

      setScopeResult(false);

      Stream<Sensitive> sensitiveStream1 = bean.streamSensitiveReturningMethod();
      sensitiveStream1.count();

      Stream<Sensitive> sensitiveStream2 = bean.streamSensitiveReturningMethod();
      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> sensitiveStream2.collect(toList()))
            .withMessage(
                  "Scope check failed for 'Sensitive[]' value in return object of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::streamSensitiveReturningMethod' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithStreamSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithStreamSensitiveParameter(Stream.of(new Sensitive()));

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithStreamSensitiveParameter(Stream.of(new Sensitive())))
            .withMessage(
                  "Scope check failed for 'Sensitive[]' value in 'parameter' parameter of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::methodWithStreamSensitiveParameter' operation using 'null' scope context");
   }

   @Test
   public void testMyIterableSensitiveReturningMethod() {
      setScopeResult(true);

      bean.myIterableSensitiveReturningMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.myIterableSensitiveReturningMethod())
            .withMessage(
                  "Scope check failed for '[Sensitive[]]' return object of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::myIterableSensitiveReturningMethod' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithMyIterableSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithMyIterableSensitiveParameter(new MyIterable<>(new Sensitive()));

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithMyIterableSensitiveParameter(new MyIterable<>(new Sensitive())))
            .withMessage(
                  "Scope check failed for 'parameter=[Sensitive[]]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::methodWithMyIterableSensitiveParameter' operation using 'null' scope context");
   }

   @Test
   public void testMySensitiveIterableReturningMethod() {
      setScopeResult(true);

      bean.mySensitiveIterableReturningMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.mySensitiveIterableReturningMethod())
            .withMessage(
                  "Scope check failed for '[Sensitive[]]' return object of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::mySensitiveIterableReturningMethod' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithMySensitiveIterableParameter() {
      setScopeResult(true);

      bean.methodWithMySensitiveIterableParameter(new MySensitiveIterable(new Sensitive()));

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithMySensitiveIterableParameter(new MySensitiveIterable(new Sensitive())))
            .withMessage(
                  "Scope check failed for 'parameter=[Sensitive[]]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::methodWithMySensitiveIterableParameter' operation using 'null' scope context");
   }

   @Test
   public void testComposedSensitiveReturningMethod() {
      setScopeResult(true);

      bean.composedSensitiveReturningMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.composedSensitiveReturningMethod())
            .withMessage(
                  "Scope check failed for 'Optional[[[[Sensitive[]]]]]' return object of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::composedSensitiveReturningMethod' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithComposedSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithComposedSensitiveParameter(optional(Arrays.asList(new MyIterable<>(new MySensitiveIterable(
            new Sensitive())))));

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithComposedSensitiveParameter(optional(Arrays.asList(new MyIterable<>(
                  new MySensitiveIterable(new Sensitive()))))))
            .withMessage(
                  "Scope check failed for 'parameter=Optional[[[[Sensitive[]]]]]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::methodWithComposedSensitiveParameter' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithOptionalGenericSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithOptionalGenericSensitiveParameter(optional(new Sensitive()));

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithOptionalGenericSensitiveParameter(optional(new Sensitive())))
            .withMessage(
                  "Scope check failed for 'parameter=Optional[Sensitive[]]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::methodWithOptionalGenericSensitiveParameter' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithGenericOptionalSensitiveParameter() {
      setScopeResult(true);

      bean.methodWithGenericOptionalSensitiveParameter(optional(new Sensitive()));

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithGenericOptionalSensitiveParameter(optional(new Sensitive())))
            .withMessage(
                  "Scope check failed for 'parameter=Optional[Sensitive[]]' parameter of 'com.tinubu.wallkeeper.aspect.trigger.AnnotatedBeanWithAnnotatedType::methodWithGenericOptionalSensitiveParameter' operation using 'null' scope context");
   }

}