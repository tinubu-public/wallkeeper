/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.supertype;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.type.ExtendingRegistered;
import com.tinubu.wallkeeper.type.SuperType;

@Component
public class SuperTypedBean {
   @Scoped
   public SuperType superTypeReturningMethod() {
      return new ExtendingRegistered();
   }

   public void methodWithSuperTypeParameter(@Scoped SuperType parameter) {
   }

   @Scoped
   public Optional<SuperType> optionalSuperTypeReturningMethod() {
      return optional(new ExtendingRegistered());
   }

   public void methodWithOptionalSuperTypeParameter(@Scoped Optional<SuperType> parameter) {
   }

   @Scoped
   public List<SuperType> iterableSuperTypeReturningMethod() {
      return Arrays.asList(new ExtendingRegistered());
   }

   public void methodWithIterableSuperTypeParameter(@Scoped List<SuperType> parameter) {
   }

}