/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.supertype;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.tinubu.wallkeeper.ParameterScopeException;
import com.tinubu.wallkeeper.ReturnObjectScopeException;
import com.tinubu.wallkeeper.aspect.AbstractScopeAspectTest;
import com.tinubu.wallkeeper.type.ExtendingRegistered;

@ContextConfiguration(classes = SuperTypedBeanAspectTest.class)
public class SuperTypedBeanAspectTest extends AbstractScopeAspectTest {

   @Autowired
   SuperTypedBean bean;

   @Test
   public void testSuperTypeReturningMethod() {
      setScopeResult(true);

      bean.superTypeReturningMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.superTypeReturningMethod())
            .withMessage(
                  "Scope check failed for 'ExtendingRegistered[value='any']' return object of 'com.tinubu.wallkeeper.aspect.supertype.SuperTypedBean::superTypeReturningMethod' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithSuperTypeParameter() {
      setScopeResult(true);

      bean.methodWithSuperTypeParameter(new ExtendingRegistered());

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithSuperTypeParameter(new ExtendingRegistered()))
            .withMessage(
                  "Scope check failed for 'parameter=ExtendingRegistered[value='any']' parameter of 'com.tinubu.wallkeeper.aspect.supertype.SuperTypedBean::methodWithSuperTypeParameter' operation using 'null' scope context");
   }

   @Test
   public void testOptionalSuperTypeReturningMethod() {
      setScopeResult(true);

      bean.optionalSuperTypeReturningMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.optionalSuperTypeReturningMethod())
            .withMessage(
                  "Scope check failed for 'Optional[ExtendingRegistered[value='any']]' return object of 'com.tinubu.wallkeeper.aspect.supertype.SuperTypedBean::optionalSuperTypeReturningMethod' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithOptionalSuperTypeParameter() {
      setScopeResult(true);

      bean.methodWithOptionalSuperTypeParameter(optional(new ExtendingRegistered()));

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithOptionalSuperTypeParameter(optional(new ExtendingRegistered())))
            .withMessage(
                  "Scope check failed for 'parameter=Optional[ExtendingRegistered[value='any']]' parameter of 'com.tinubu.wallkeeper.aspect.supertype.SuperTypedBean::methodWithOptionalSuperTypeParameter' operation using 'null' scope context");
   }

   @Test
   public void testIterableSuperTypeReturningMethod() {
      setScopeResult(true);

      bean.iterableSuperTypeReturningMethod();

      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.iterableSuperTypeReturningMethod())
            .withMessage(
                  "Scope check failed for '[ExtendingRegistered[value='any']]' return object of 'com.tinubu.wallkeeper.aspect.supertype.SuperTypedBean::iterableSuperTypeReturningMethod' operation using 'null' scope context");
   }

   @Test
   public void testMethodWithIterableSuperTypeParameter() {
      setScopeResult(true);

      bean.methodWithIterableSuperTypeParameter(Arrays.asList(new ExtendingRegistered()));

      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.methodWithIterableSuperTypeParameter(Arrays.asList(new ExtendingRegistered())))
            .withMessage(
                  "Scope check failed for 'parameter=[ExtendingRegistered[value='any']]' parameter of 'com.tinubu.wallkeeper.aspect.supertype.SuperTypedBean::methodWithIterableSuperTypeParameter' operation using 'null' scope context");
   }

}