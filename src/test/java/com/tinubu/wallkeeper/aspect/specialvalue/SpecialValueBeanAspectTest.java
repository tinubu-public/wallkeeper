/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.specialvalue;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.tinubu.wallkeeper.ParameterScopeException;
import com.tinubu.wallkeeper.ReturnObjectScopeException;
import com.tinubu.wallkeeper.aspect.AbstractScopeAspectTest;
import com.tinubu.wallkeeper.type.Registered;

@ContextConfiguration(classes = SpecialValueBeanAspectTest.class)
public class SpecialValueBeanAspectTest extends AbstractScopeAspectTest {

   @Autowired
   MirrorBean bean;

   @Test
   public void testNullValue() {
      setScopeResult(true);

      bean.mirrorMethod((Registered) null);
      bean.mirrorMethod((Optional<Registered>) null);
      bean.mirrorMethod((List<Registered>) null);

      setScopeResult(false);

      bean.mirrorMethod((Registered) null);
      bean.mirrorMethod((Optional<Registered>) null);
      bean.mirrorMethod((List<Registered>) null);
   }

   @Test
   public void testNullParameterWithSpelExpression() {
      setScopeResult(true);

      bean.nullParameter((Registered) null);

      setScopeResult(false);

      Assertions
            .assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.nullParameter((Registered) null))
            .withMessage(
                  "Scope check failed for 'Registered[value='any']' return object of 'com.tinubu.wallkeeper.aspect.specialvalue.MirrorBean::nullParameter' operation using 'null' scope context");
   }

   @Test
   public void testEmptyOptionalValue() {
      setScopeResult(true);

      bean.mirrorMethod(optional());

      setScopeResult(false);

      bean.mirrorMethod(optional());
   }

   @Test
   public void testEmptyListValue() {
      setScopeResult(true);

      bean.mirrorMethod(Arrays.asList());

      setScopeResult(false);

      bean.mirrorMethod(Arrays.asList());
   }

   @Test
   public void testEmptyStreamValue() {
      setScopeResult(true);

      bean.mirrorMethod(Stream.of());

      setScopeResult(false);

      bean.mirrorMethod(Stream.of());
   }

   @Test
   public void testUntypedOptionalValue() {
      setScopeResult(true);

      bean.untypedMirrorMethod(optional(new Registered()));

      setScopeResult(false);

      Assertions
            .assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.untypedMirrorMethod(optional(new Registered())))
            .withMessage(
                  "Scope check failed for 'parameter=Optional[Registered[value='any']]' parameter of 'com.tinubu.wallkeeper.aspect.specialvalue.MirrorBean::untypedMirrorMethod' operation using 'null' scope context");
   }

   @Test
   public void testUntypedIterableValue() {
      setScopeResult(true);

      bean.untypedMirrorMethod(Arrays.asList(new Registered()));

      setScopeResult(false);

      Assertions
            .assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.untypedMirrorMethod(Arrays.asList(new Registered())))
            .withMessage(
                  "Scope check failed for 'parameter=[Registered[value='any']]' parameter of 'com.tinubu.wallkeeper.aspect.specialvalue.MirrorBean::untypedMirrorMethod' operation using 'null' scope context");
   }

   @Test
   @Disabled("FIXME")
   public void testUntypedStreamValue() {
      setScopeResult(true);

      bean.untypedMirrorMethod(Stream.of(new Registered()));

      setScopeResult(false);

      Assertions
            .assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.untypedMirrorMethod(Stream.of(new Registered())).count())
            .withMessage(
                  "Scope check failed for 'parameter=[Registered[value='any']]' parameter of 'com.tinubu.wallkeeper.aspect.specialvalue.MirrorBean::untypedMirrorMethod' operation using 'null' scope context");
   }

}