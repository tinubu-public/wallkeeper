/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.specification;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.tinubu.wallkeeper.aspect.AbstractScopeAspectTest;

@ContextConfiguration(classes = AnnotatedSpecificationBeanAspectTest.class)
public class AnnotatedSpecificationBeanAspectTest extends AbstractScopeAspectTest {

   @Autowired
   AnnotatedSpecificationBean bean;

   @Test
   public void testMethodWithRegisteredSpecificationParameter() {
      setScopeResult(true);

      assertThat(bean.methodWithRegisteredSpecificationParameter(registered -> true)).isTrue();

      setScopeResult(false);

      assertThat(bean.methodWithRegisteredSpecificationParameter(registered -> true)).isTrue();
   }

   @Test
   public void testMethodWithUnRegisteredSpecificationParameter() {
      setScopeResult(true);

      assertThat(bean.methodWithUnRegisteredSpecificationParameter(unRegistered -> true)).isTrue();

      setScopeResult(false);

      assertThat(bean.methodWithUnRegisteredSpecificationParameter(unRegistered -> true)).isTrue();
   }

   @Test
   public void testMethodWithSensitiveSpecificationParameter() {
      setScopeResult(true);

      assertThat(bean.methodWithSensitiveSpecificationParameter(unRegistered -> true)).isTrue();

      setScopeResult(false);

      assertThat(bean.methodWithSensitiveSpecificationParameter(unRegistered -> true)).isFalse();
   }

   @Test
   public void testMethodWithNonSensitiveSpecificationParameter() {
      setScopeResult(true);

      assertThat(bean.methodWithNonSensitiveSpecificationParameter(unRegistered -> true)).isTrue();

      setScopeResult(false);

      assertThat(bean.methodWithNonSensitiveSpecificationParameter(unRegistered -> true)).isTrue();
   }

   @Test
   public void testMethodWithIgnoredSensitiveSpecificationParameter() {
      setScopeResult(true);

      assertThat(bean.methodWithIgnoredSensitiveSpecificationParameter(unRegistered -> true)).isTrue();

      setScopeResult(false);

      assertThat(bean.methodWithIgnoredSensitiveSpecificationParameter(unRegistered -> true)).isTrue();
   }

   @Test
   public void testMethodWithAnnotatedNonSensitiveSpecificationParameter() {
      setScopeResult(true);

      assertThat(bean.methodWithAnnotatedNonSensitiveSpecificationParameter(unRegistered -> true)).isTrue();

      setScopeResult(false);

      assertThat(bean.methodWithAnnotatedNonSensitiveSpecificationParameter(unRegistered -> true)).isFalse();
   }

}