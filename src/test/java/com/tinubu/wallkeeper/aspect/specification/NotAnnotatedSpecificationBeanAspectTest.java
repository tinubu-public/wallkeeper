/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.specification;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.wallkeeper.NotRegisteredScopeException;
import com.tinubu.wallkeeper.aspect.AbstractScopeAspectTest;
import com.tinubu.wallkeeper.aspect.ScopeConfig;
import com.tinubu.wallkeeper.type.Registered;
import com.tinubu.wallkeeper.type.UnRegistered;

@ContextConfiguration(classes = NotAnnotatedSpecificationBeanAspectTest.class)
public class NotAnnotatedSpecificationBeanAspectTest extends AbstractScopeAspectTest {

   @Autowired
   NotAnnotatedSpecificationBean bean;

   @Test
   public void testSpecificationWhenNullAndRegistered() {
      setScopeResult(true);

      assertThat(bean.methodWithRegisteredSpecificationParameter(null)).isTrue();

      setScopeResult(false);

      assertThat(bean.methodWithRegisteredSpecificationParameter(null)).isFalse();
   }

   @Test
   public void testSpecificationWhenNullAndUnRegistered() {
      setScopeResult(true);

      assertThatExceptionOfType(NotRegisteredScopeException.class)
            .isThrownBy(() -> bean.methodWithUnRegisteredSpecificationParameter(null))
            .withMessage("Scope not registered for 'com.tinubu.wallkeeper.type.UnRegistered'");
   }

   @Test
   public void testMethodWithRegisteredSpecificationParameter() {
      setScopeResult(true);

      assertThat(bean.methodWithRegisteredSpecificationParameter(registered -> true)).isTrue();

      setScopeResult(false);

      assertThat(bean.methodWithRegisteredSpecificationParameter(registered -> true)).isFalse();
   }

   @Test
   public void testMethodWithUnRegisteredSpecificationParameter() {
      setScopeResult(true);

      assertThat(bean.methodWithUnRegisteredSpecificationParameter(unRegistered -> true)).isTrue();

      setScopeResult(false);

      assertThat(bean.methodWithUnRegisteredSpecificationParameter(unRegistered -> true)).isTrue();
   }

   @Test
   public void testRegisteredSpecificationReturningMethod() {
      ScopeConfig.scopeFactory.registerDefaultScope(Specification.class);

      setScopeResult(true);

      assertThat(bean.registeredSpecificationReturningMethod()).accepts(new Registered());

      setScopeResult(false);

      assertThat(bean.registeredSpecificationReturningMethod()).accepts(new Registered());
   }

   @Test
   public void testUnRegisteredSpecificationReturningMethod() {
      ScopeConfig.scopeFactory.registerDefaultScope(Specification.class);

      setScopeResult(true);

      assertThat(bean.unRegisteredSpecificationReturningMethod()).accepts(new UnRegistered());

      setScopeResult(false);

      assertThat(bean.unRegisteredSpecificationReturningMethod()).accepts(new UnRegistered());
   }

   @Test
   public void testMethodWithRegisteredCompositeSpecificationParameter() {
      setScopeResult(true);

      assertThat(bean.methodWithRegisteredCompositeSpecificationParameter(registered -> true)).isTrue();

      setScopeResult(false);

      assertThat(bean.methodWithRegisteredCompositeSpecificationParameter(registered -> true)).isFalse();
   }

   @Test
   public void testMethodWithUnRegisteredCompositeSpecificationParameter() {
      setScopeResult(true);

      assertThat(bean.methodWithUnRegisteredCompositeSpecificationParameter(unRegistered -> true)).isTrue();

      setScopeResult(false);

      assertThat(bean.methodWithUnRegisteredCompositeSpecificationParameter(unRegistered -> true)).isTrue();
   }

   @Test
   public void testRegisteredCompositeSpecificationReturningMethod() {
      ScopeConfig.scopeFactory.registerDefaultScope(Specification.class);

      setScopeResult(true);

      assertThat(bean.registeredCompositeSpecificationReturningMethod()).accepts(new Registered());

      setScopeResult(false);

      assertThat(bean.registeredCompositeSpecificationReturningMethod()).accepts(new Registered());
   }

   @Test
   public void testUnRegisteredCompositeSpecificationReturningMethod() {
      ScopeConfig.scopeFactory.registerDefaultScope(Specification.class);

      setScopeResult(true);

      assertThat(bean.unRegisteredCompositeSpecificationReturningMethod()).accepts(new UnRegistered());

      setScopeResult(false);

      assertThat(bean.unRegisteredCompositeSpecificationReturningMethod()).accepts(new UnRegistered());
   }

}