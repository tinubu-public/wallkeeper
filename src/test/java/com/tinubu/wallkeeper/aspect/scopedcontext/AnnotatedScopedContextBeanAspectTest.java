/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.scopedcontext;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import com.tinubu.wallkeeper.ParameterScopeException;
import com.tinubu.wallkeeper.aspect.AbstractScopeAspectTest;
import com.tinubu.wallkeeper.type.Sensitive;

@ContextConfiguration(classes = AnnotatedScopedContextBeanAspectTest.class)
public class AnnotatedScopedContextBeanAspectTest extends AbstractScopeAspectTest {

   @Autowired
   AnnotatedScopedContextBean bean;

   @Test
   public void testScopedContextBeanMethod() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedContextBeanMethod(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.scopedcontext.AnnotatedScopedContextBean::scopedContextBeanMethod' operation using 'TestScopeContext[type=READ, data=null]' scope context");
   }

   @Test
   public void testScopedContextBeanMethodWithMethodOverride() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedContextBeanMethodWithMethodOverride(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.scopedcontext.AnnotatedScopedContextBean::scopedContextBeanMethodWithMethodOverride' operation using 'TestScopeContext[type=WRITE, data=null]' scope context");
   }

   @Test
   public void testScopedContextBeanMethodWithParameterOverride() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedContextBeanMethodWithParameterOverride(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.scopedcontext.AnnotatedScopedContextBean::scopedContextBeanMethodWithParameterOverride' operation using 'TestScopeContext[type=WRITE, data=null]' scope context");
   }

   @Test
   public void testScopedContextBeanMethodWithMethodAndParameterOverride() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedContextBeanMethodWithMethodAndParameterOverride(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.scopedcontext.AnnotatedScopedContextBean::scopedContextBeanMethodWithMethodAndParameterOverride' operation using 'TestScopeContext[type=WRITE, data=null]' scope context");
   }

}