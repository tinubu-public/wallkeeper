/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.scopedcontext;

import org.springframework.stereotype.Component;

import com.tinubu.wallkeeper.Scoped;
import com.tinubu.wallkeeper.ScopedBean;
import com.tinubu.wallkeeper.ScopedContext;
import com.tinubu.wallkeeper.type.Sensitive;

@Component
@ScopedBean
public class ScopedContextBean {

   public void defaultScopedContext(Sensitive parameter) {
   }

   public Sensitive defaultScopedContext() {
      return new Sensitive();
   }

   public void scopedDefaultScopedContext(@Scoped(ctx = @ScopedContext()) Sensitive parameter) {
   }

   public @Scoped(ctx = @ScopedContext())
   Sensitive scopedDefaultScopedContext() {
      return new Sensitive();
   }

   public void scopedScopedContext(@Scoped(ctx = @ScopedContext(
         "new com.tinubu.wallkeeper.aspect.TestScopeContext(T(com.tinubu.wallkeeper.aspect.TestScopeContext$Type).READ)"))
                                         Sensitive parameter) {
   }

   public @Scoped(ctx = @ScopedContext(
         "new com.tinubu.wallkeeper.aspect.TestScopeContext(T(com.tinubu.wallkeeper.aspect.TestScopeContext$Type).READ)"))
   Sensitive scopedScopedContext() {
      return new Sensitive();
   }

   public void scopedInvalidScopedContext(
         @Scoped(ctx = @ScopedContext("##INVALID SpEL##")) Sensitive parameter) {
   }

   public @Scoped(ctx = @ScopedContext("##INVALID SpEL##"))
   Sensitive scopedInvalidScopedContext() {
      return new Sensitive();
   }

   public void scopedBeanReferencingScopedContext(
         @Scoped(ctx = @ScopedContext("@scopeContext.read()")) Sensitive parameter) {
   }

   public @Scoped(ctx = @ScopedContext("@scopeContext.read()"))
   Sensitive scopedBeanReferencingScopedContext() {
      return new Sensitive();
   }

   public void scopedObjectReferencingScopedContext(
         @Scoped(ctx = @ScopedContext("@scopeContext.read(scopedObject)")) Sensitive parameter) {
   }

   public @Scoped(ctx = @ScopedContext("@scopeContext.read(scopedObject)"))
   Sensitive scopedObjectReferencingScopedContext() {
      return new Sensitive();
   }

   public void scopedParameterReferencingScopedContext(
         @Scoped(ctx = @ScopedContext("@scopeContext.read(#hint)")) Sensitive parameter, String hint) {
   }

   public @Scoped(ctx = @ScopedContext("@scopeContext.read(#hint)"))
   Sensitive scopedParameterReferencingScopedContext(String hint) {
      return new Sensitive();
   }

   @Scoped(ctx = @ScopedContext("@scopeContext.read()"))
   public void scopedScopedContextMethod(Sensitive parameter) {
   }

   @ScopedContext("@scopeContext.read()")
   public void scopedContextMethod(Sensitive parameter) {
   }

   @ScopedContext("@scopeContext.read()")
   public void scopedContextMethodWithOverride(
         @Scoped(ctx = @ScopedContext("@scopeContext.write()")) Sensitive parameter) {
   }

}

