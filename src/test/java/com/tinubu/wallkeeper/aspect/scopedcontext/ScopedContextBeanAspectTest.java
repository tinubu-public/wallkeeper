/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.scopedcontext;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.SpelParseException;
import org.springframework.test.context.ContextConfiguration;

import com.tinubu.wallkeeper.ParameterScopeException;
import com.tinubu.wallkeeper.ReturnObjectScopeException;
import com.tinubu.wallkeeper.aspect.AbstractScopeAspectTest;
import com.tinubu.wallkeeper.type.Sensitive;

@ContextConfiguration(classes = ScopedContextBeanAspectTest.class)
public class ScopedContextBeanAspectTest extends AbstractScopeAspectTest {

   @Autowired
   ScopedContextBean bean;

   @Test
   public void testDefaultScopedContext() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.defaultScopedContext(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.scopedcontext.ScopedContextBean::defaultScopedContext' operation using 'null' scope context");
      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.defaultScopedContext())
            .withMessage(
                  "Scope check failed for 'Sensitive[]' return object of 'com.tinubu.wallkeeper.aspect.scopedcontext.ScopedContextBean::defaultScopedContext' operation using 'null' scope context");
   }

   @Test
   public void testScopedDefaultScopedContext() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedDefaultScopedContext(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.scopedcontext.ScopedContextBean::scopedDefaultScopedContext' operation using 'null' scope context");
      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.scopedDefaultScopedContext())
            .withMessage(
                  "Scope check failed for 'Sensitive[]' return object of 'com.tinubu.wallkeeper.aspect.scopedcontext.ScopedContextBean::scopedDefaultScopedContext' operation using 'null' scope context");
   }

   @Test
   public void testScopedScopedContext() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedScopedContext(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.scopedcontext.ScopedContextBean::scopedScopedContext' operation using 'TestScopeContext[type=READ, data=null]' scope context");
      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.scopedScopedContext())
            .withMessage(
                  "Scope check failed for 'Sensitive[]' return object of 'com.tinubu.wallkeeper.aspect.scopedcontext.ScopedContextBean::scopedScopedContext' operation using 'TestScopeContext[type=READ, data=null]' scope context");
   }

   @Test
   public void testScopedInvalidScopedContext() {
      setScopeResult(false);

      assertThatExceptionOfType(SpelParseException.class).isThrownBy(() -> bean.scopedInvalidScopedContext(new Sensitive()));
      assertThatExceptionOfType(SpelParseException.class).isThrownBy(() -> bean.scopedInvalidScopedContext());
   }

   @Test
   public void testScopedBeanReferencingScopedContext() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedBeanReferencingScopedContext(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.scopedcontext.ScopedContextBean::scopedBeanReferencingScopedContext' operation using 'TestScopeContext[type=READ, data=null]' scope context");
      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.scopedBeanReferencingScopedContext())
            .withMessage(
                  "Scope check failed for 'Sensitive[]' return object of 'com.tinubu.wallkeeper.aspect.scopedcontext.ScopedContextBean::scopedBeanReferencingScopedContext' operation using 'TestScopeContext[type=READ, data=null]' scope context");
   }

   @Test
   public void testScopedObjectReferencingScopedContext() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedObjectReferencingScopedContext(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.scopedcontext.ScopedContextBean::scopedObjectReferencingScopedContext' operation using 'TestScopeContext[type=READ, data=Sensitive[]]' scope context");
      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.scopedObjectReferencingScopedContext())
            .withMessage(
                  "Scope check failed for 'Sensitive[]' return object of 'com.tinubu.wallkeeper.aspect.scopedcontext.ScopedContextBean::scopedObjectReferencingScopedContext' operation using 'TestScopeContext[type=READ, data=Sensitive[]]' scope context");
   }

   @Test
   public void testScopedParameterReferencingScopedContext() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedParameterReferencingScopedContext(new Sensitive(), "HINT"))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.scopedcontext.ScopedContextBean::scopedParameterReferencingScopedContext' operation using 'TestScopeContext[type=READ, data=HINT]' scope context");
      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.scopedParameterReferencingScopedContext("HINT"))
            .withMessage(
                  "Scope check failed for 'Sensitive[]' return object of 'com.tinubu.wallkeeper.aspect.scopedcontext.ScopedContextBean::scopedParameterReferencingScopedContext' operation using 'TestScopeContext[type=READ, data=HINT]' scope context");
   }

   @Test
   public void testScopedScopedContextMethod() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedScopedContextMethod(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.scopedcontext.ScopedContextBean::scopedScopedContextMethod' operation using 'null' scope context");
   }

   @Test
   public void testScopedContextMethod() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedContextMethod(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.scopedcontext.ScopedContextBean::scopedContextMethod' operation using 'TestScopeContext[type=READ, data=null]' scope context");
   }

   @Test
   public void testScopedContextMethodWithOverride() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedContextMethodWithOverride(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.scopedcontext.ScopedContextBean::scopedContextMethodWithOverride' operation using 'TestScopeContext[type=WRITE, data=null]' scope context");
   }

}