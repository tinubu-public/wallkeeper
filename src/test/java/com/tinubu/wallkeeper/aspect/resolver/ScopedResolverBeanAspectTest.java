/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.aspect.resolver;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.Arrays;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.SpelParseException;
import org.springframework.test.context.ContextConfiguration;

import com.tinubu.wallkeeper.ParameterScopeException;
import com.tinubu.wallkeeper.ReturnObjectScopeException;
import com.tinubu.wallkeeper.aspect.AbstractScopeAspectTest;
import com.tinubu.wallkeeper.type.Sensitive;

@ContextConfiguration(classes = ScopedResolverBeanAspectTest.class)
public class ScopedResolverBeanAspectTest extends AbstractScopeAspectTest {

   @Autowired
   ScopedResolverBean bean;

   @Test
   public void testScopedDefaultScopedResolver() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedDefaultScopedResolver(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedDefaultScopedResolver' operation using 'null' scope context");
      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.scopedDefaultScopedResolver())
            .withMessage(
                  "Scope check failed for 'Sensitive[]' return object of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedDefaultScopedResolver' operation using 'null' scope context");
   }

   @Test
   public void testScopedScopedResolver() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedScopedResolver(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]->Registered[value='any']' parameter of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedScopedResolver' operation using 'null' scope context");
      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.scopedScopedResolver())
            .withMessage(
                  "Scope check failed for 'Sensitive[]->Registered[value='any']' return object of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedScopedResolver' operation using 'null' scope context");
   }

   @Test
   public void testScopedInvalidScopedResolver() {
      setScopeResult(false);

      assertThatExceptionOfType(SpelParseException.class).isThrownBy(() -> bean.scopedInvalidScopedResolver(
            new Sensitive()));
      assertThatExceptionOfType(SpelParseException.class).isThrownBy(() -> bean.scopedInvalidScopedResolver());
   }

   @Test
   public void testScopedBeanReferencingScopedResolver() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedBeanReferencingScopedResolver(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]->Registered[value='any']' parameter of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedBeanReferencingScopedResolver' operation using 'null' scope context");
      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.scopedBeanReferencingScopedResolver())
            .withMessage(
                  "Scope check failed for 'Sensitive[]->Registered[value='any']' return object of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedBeanReferencingScopedResolver' operation using 'null' scope context");
   }

   @Test
   public void testScopedObjectReferencingScopedResolver() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedObjectReferencingScopedResolver(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedObjectReferencingScopedResolver' operation using 'null' scope context");
      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.scopedObjectReferencingScopedResolver())
            .withMessage(
                  "Scope check failed for 'Sensitive[]' return object of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedObjectReferencingScopedResolver' operation using 'null' scope context");
   }

   @Test
   public void testScopedParameterReferencingScopedResolver() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedParameterReferencingScopedResolver(new String(), new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=->Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedParameterReferencingScopedResolver' operation using 'null' scope context");
      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean.scopedParameterReferencingScopedResolver(new Sensitive()))
            .withMessage(
                  "Scope check failed for '->Sensitive[]' return object of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedParameterReferencingScopedResolver' operation using 'null' scope context");
   }

   @Test
   public void testScopedScopedResolverMethod() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedScopedResolverMethod(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]' parameter of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedScopedResolverMethod' operation using 'null' scope context");
   }

   @Test
   public void testScopedResolverMethod() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedResolverMethod(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]->Registered[value='any']' parameter of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedResolverMethod' operation using 'null' scope context");
   }

   @Test
   public void testScopedResolverMethodWithOverride() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedResolverMethodWithOverride(new Sensitive()))
            .withMessage(
                  "Scope check failed for 'parameter=Sensitive[]->Registered[value='any']' parameter of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedResolverMethodWithOverride' operation using 'null' scope context");
   }

   @Test
   public void testScopedResolverWhenOptionalParameter() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedResolverWhenOptionalParameter(optional(new Sensitive())))
            .withMessage(
                  "Scope check failed for 'parameter=Optional[Sensitive[]]->Optional[Registered[value='any']]' parameter of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedResolverWhenOptionalParameter' operation using 'null' scope context");

      bean.scopedResolverWhenOptionalParameter(optional());
   }

   @Test
   public void testScopedResolverWhenIterableParameter() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedResolverWhenIterableParameter(Arrays.asList(new Sensitive(),
                                                                                     new Sensitive())))
            .withMessage(
                  "Scope check failed for 'parameter=[Sensitive[], Sensitive[]]->[Registered[value='any'], Registered[value='any']]' parameter of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedResolverWhenIterableParameter' operation using 'null' scope context");

      bean.scopedResolverWhenIterableParameter(emptyList());
   }

   @Test
   public void testScopedResolverWhenComposedParameter() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedResolverWhenComposedParameter(optional(Arrays.asList(new Sensitive(),
                                                                                              new Sensitive()))))
            .withMessage(
                  "Scope check failed for 'parameter=Optional[[Sensitive[], Sensitive[]]]->Optional[[Registered[value='any'], Registered[value='any']]]' parameter of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedResolverWhenComposedParameter' operation using 'null' scope context");

      bean.scopedResolverWhenComposedParameter(optional(emptyList()));

      setScopeResult(true);

      bean.scopedResolverWhenComposedParameter(optional(Arrays.asList(new Sensitive(), new Sensitive())));
   }

   @Test
   public void testScopedResolverWhenStreamParameter() {
      setScopeResult(false);

      assertThatExceptionOfType(ParameterScopeException.class)
            .isThrownBy(() -> bean.scopedResolverWhenStreamParameter(Stream.of(new Sensitive(),
                                                                               new Sensitive())))
            .withMessage(
                  "Scope check failed for 'Sensitive[]->Registered[value='any']' value in 'parameter' parameter of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedResolverWhenStreamParameter' operation using 'null' scope context");

      bean.scopedResolverWhenStreamParameter(Stream.of());
   }

   @Test
   public void testScopedResolverWhenStreamReturnObject() {
      setScopeResult(false);

      assertThatExceptionOfType(ReturnObjectScopeException.class)
            .isThrownBy(() -> bean
                  .scopedResolverWhenStreamReturnObject(Stream.of(new Sensitive(), new Sensitive()))
                  .collect(toList()))
            .withMessage(
                  "Scope check failed for 'Sensitive[]->Registered[value='any']' value in return object of 'com.tinubu.wallkeeper.aspect.resolver.ScopedResolverBean::scopedResolverWhenStreamReturnObject' operation using 'null' scope context");

      bean.scopedResolverWhenStreamReturnObject(Stream.of());
   }

}