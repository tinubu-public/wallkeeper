/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.wallkeeper.aspect.ScopeConfig.TestUserScope;
import com.tinubu.wallkeeper.type.ExtendingRegistered;
import com.tinubu.wallkeeper.type.Registered;

class ScopeFactoryTest {

   @Test
   public void testScopeWhenNominal() {
      ScopeFactory<ScopeContext, UserScope> scopeFactory = new ScopeFactory<>();

      scopeFactory.registerScope(Registered.class, (context, userScope) -> object -> true);

      Optional<Specification<Registered>> scope =
            scopeFactory.scope(Registered.class, null, optional(new TestUserScope()));

      assertThat(scope).isPresent();
      assertThat(scope).get().satisfies(s -> assertThat(s.satisfiedBy(new Registered())).isTrue());
      assertThat(scope).get().satisfies(s -> assertThat(s.satisfiedBy(null)).isTrue());
   }

   @Test
   public void testScopeWhenNotRegistered() {
      ScopeFactory<ScopeContext, UserScope> scopeFactory = new ScopeFactory<>();

      assertThat(scopeFactory.scope(Registered.class, null, optional(new TestUserScope()))).isEmpty();
   }

   @Test
   public void testScopeWhenFactoryGeneratesNullSpecification() {
      ScopeFactory<ScopeContext, UserScope> scopeFactory = new ScopeFactory<>();

      scopeFactory.registerScope(Registered.class, (context, userScope) -> null);

      Optional<Specification<Registered>> scope =
            scopeFactory.scope(Registered.class, null, optional(new TestUserScope()));

      assertThat(scope).isEmpty();
   }

   @Test
   public void testScopeWhenCachingScopeFactory() {
      ScopeFactory<ScopeContext, UserScope> scopeFactory = new ScopeFactory<>();

      AtomicInteger specificationGenerationCount = new AtomicInteger(0);
      scopeFactory.registerScope(Registered.class,
                                 new CachingScopeSpecificationFactory<Registered, ScopeContext, UserScope>() {
                                    @Override
                                    protected Specification<Registered> buildScopeSpecification(ScopeContext context,
                                                                                                Optional<UserScope> scope) {
                                       specificationGenerationCount.incrementAndGet();
                                       return __ -> true;
                                    }
                                 });

      Optional<Specification<Registered>> scope = null;
      for (int i = 0; i < 3; i++) {
         scope = scopeFactory.scope(Registered.class, null, optional(new TestUserScope()));
      }

      assertThat(scope).isPresent();
      assertThat(specificationGenerationCount.get()).isEqualTo(1);
   }

   @Test
   public void testScopeWhenRegisteredSuperType() {
      ScopeFactory<ScopeContext, UserScope> scopeFactory = new ScopeFactory<>();

      scopeFactory.registerScope(Registered.class, (context, userScope) -> object -> true);

      Optional<Specification<ExtendingRegistered>> scope =
            scopeFactory.scope(ExtendingRegistered.class, null, optional(new TestUserScope()));

      assertThat(scope).isPresent();
      assertThat(scope).get().satisfies(s -> assertThat(s.satisfiedBy(new ExtendingRegistered())).isTrue());
      assertThat(scope).get().satisfies(s -> assertThat(s.satisfiedBy(null)).isTrue());
   }

}