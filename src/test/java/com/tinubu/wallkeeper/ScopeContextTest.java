/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper;

import static com.tinubu.wallkeeper.ScopeContextTest.SimpleContext.READ;
import static com.tinubu.wallkeeper.ScopeContextTest.SimpleContext.WRITE;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.util.Objects;

import org.junit.jupiter.api.Test;

public class ScopeContextTest {

   @Test
   public void testCheckContextWhenNominal() {
      READ.checkContext(READ, WRITE);

      assertThatIllegalArgumentException()
            .isThrownBy(() -> READ.checkContext(WRITE))
            .withMessage("'context' must match any of 'WRITE'");
   }

   @Test
   public void testCheckContextWhenBadParameters() {
      assertThatNullPointerException()
            .isThrownBy(() -> READ.checkContext((ScopeContext[]) null))
            .withMessage("'supportedScopeContexts' must not be null");

      assertThatIllegalArgumentException()
            .isThrownBy(() -> READ.checkContext((ScopeContext) null))
            .withMessage("'supportedScopeContexts' must not have null elements at index : 0");
   }

   @Test
   public void testCheckContextWhenEmpty() {
      assertThatIllegalArgumentException()
            .isThrownBy(READ::checkContext)
            .withMessage("'context' must match any of ''");
   }

   @Test
   public void testCheckContextWhenComplexContext() {
      assertThatIllegalArgumentException()
            .isThrownBy(() -> new ComplexContext("hint").checkContext(READ))
            .withMessage("'context' must match any of 'READ'");

      new ComplexContext("hint").checkContext(new ComplexContext("hint"));

      new ComplexContext("hint").checkContext(new ComplexContext("another hint"));
   }

   public enum SimpleContext implements ScopeContext {
      READ, WRITE
   }

   public static class ComplexContext implements ScopeContext {

      private final String hint;

      public ComplexContext(String hint) {
         this.hint = hint;
      }

      @Override
      public boolean sameContextAs(ScopeContext scopeContext) {
         return scopeContext instanceof ComplexContext;
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         ComplexContext that = (ComplexContext) o;
         return Objects.equals(hint, that.hint);
      }

      @Override
      public int hashCode() {
         return Objects.hash(hint);
      }
   }

}
