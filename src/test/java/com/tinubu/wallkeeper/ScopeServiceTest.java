/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.wallkeeper.ScopeContextTest.SimpleContext.READ;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.ddd2.domain.specification.AndSpecification;
import com.tinubu.commons.ddd2.domain.specification.CompositeSpecification;
import com.tinubu.commons.ddd2.domain.specification.Specification;
import com.tinubu.wallkeeper.aspect.ScopeConfig.TestUserScope;
import com.tinubu.wallkeeper.type.Registered;
import com.tinubu.wallkeeper.type.SuperType;
import com.tinubu.wallkeeper.type.UnRegistered;

class ScopeServiceTest {

   public static final UserScope TEST_USER_SCOPE = new TestUserScope();

   @Test
   public void testScopeWhenNominal() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThat(scopeService.userScope()).hasValue(TEST_USER_SCOPE);
   }

   @Test
   public void testScopeWhenUnscopedUser() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(), mockScopeFactory());

      assertThat(scopeService.userScope()).isEmpty();
   }

   @Test
   public void testScopeWhenNullUserScope() {
      ScopeService<ScopeContext, UserScope> scopeService = new ScopeService<>(() -> null, mockScopeFactory());

      assertThatNullPointerException()
            .isThrownBy(scopeService::userScope)
            .withMessage("'userScope' must not be null");
   }

   @Test
   public void testScopeSpecificationByClassWhenNominal() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThat(scopeService.scopeSpecification(Registered.class, READ)).hasValueSatisfying(s -> {
         assertThat(s.satisfiedBy(new Registered())).isFalse();
         assertThat(s.satisfiedBy(new Registered("inScope"))).isTrue();
      });
   }

   @Test
   public void testScopeSpecificationByClassWhenUnregisteredClass() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThat(scopeService.scopeSpecification(UnRegistered.class, READ)).isEmpty();
   }

   @Test
   public void testScopeSpecificationByClassWhenNullObjectClass() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThatNullPointerException()
            .isThrownBy(() -> scopeService.scopeSpecification(null, READ))
            .withMessage("'objectClass' must not be null");
   }

   @Test
   public void testScopeSpecificationByClassWhenNullScopeContext() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThat(scopeService.scopeSpecification(Registered.class, null)).hasValueSatisfying(s -> {
         assertThat(s.satisfiedBy(new Registered())).isFalse();
         assertThat(s.satisfiedBy(new Registered("inScope"))).isTrue();
      });
   }

   @Test
   public void testScopeSpecificationByObjectWhenNominal() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThat(scopeService.scopeSpecification(new Registered("any"), READ)).hasValueSatisfying(s -> {
         assertThat(s.satisfiedBy(new Registered())).isFalse();
         assertThat(s.satisfiedBy(new Registered("inScope"))).isTrue();
      });
   }

   @Test
   public void testScopeSpecificationByObjectWhenPassingSuperType() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      SuperType superType = new Registered();

      assertThat(scopeService.scopeSpecification(superType,
                                                 READ)).hasValueSatisfying(s -> assertThat(s.satisfiedBy(new Registered(
            "inScope"))).isTrue());
   }

   @Test
   public void testScopeSpecificationByObjectWhenUnregisteredClass() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThat(scopeService.scopeSpecification(new UnRegistered(), READ)).isEmpty();
   }

   @Test
   public void testScopeSpecificationByObjectWhenNullObjectClass() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThatNullPointerException()
            .isThrownBy(() -> scopeService.scopeSpecification((Registered) null, READ))
            .withMessage("'object' must not be null");
   }

   @Test
   public void testScopeSpecificationByObjectWhenNullScopeContext() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThat(scopeService.scopeSpecification(new Registered("inScope"),
                                                 null)).hasValueSatisfying(s -> assertThat(s.satisfiedBy(new Registered(
            "inScope"))).isTrue());
   }

   @Test
   public void testScopedSpecificationBySpecificationWhenNominal() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      Specification<Registered> currentSpecification = __ -> true;
      CompositeSpecification<Registered> scopedSpecification =
            scopeService.scopedSpecification(currentSpecification, Registered.class, READ);

      assertThat(scopedSpecification).isNotNull();
      assertThat(scopedSpecification.satisfiedBy(new Registered())).isFalse();
   }

   @Test
   public void testScopedSpecificationBySpecificationWhenCompositeSpecification() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      CompositeSpecification<Registered> currentSpecification = __ -> true;
      CompositeSpecification<Registered> scopedSpecification =
            scopeService.scopedSpecification(currentSpecification, Registered.class, READ);

      assertThat(scopedSpecification).isNotNull();
      assertThat(scopedSpecification).isInstanceOf(AndSpecification.class);
      assertThat(scopedSpecification.satisfiedBy(new Registered())).isFalse();
   }

   @Test
   public void testScopedSpecificationBySpecificationWhenUnregisteredClass() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      Specification<UnRegistered> currentSpecification = __ -> true;
      CompositeSpecification<UnRegistered> scopedSpecification =
            scopeService.scopedSpecification(currentSpecification, UnRegistered.class, READ);

      assertThat(scopedSpecification).isNotNull();
      assertThat(scopedSpecification.satisfiedBy(new UnRegistered())).isTrue();
   }

   @Test
   public void testScopedSpecificationBySpecificationWhenNullSpecification() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThatNullPointerException()
            .isThrownBy(() -> scopeService.scopedSpecification((Specification<Registered>) null,
                                                               Registered.class,
                                                               READ))
            .withMessage("'specification' must not be null");
   }

   @Test
   public void testScopedSpecificationBySpecificationWhenNullObjectClass() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      Specification<Registered> currentSpecification = __ -> true;

      assertThatNullPointerException()
            .isThrownBy(() -> scopeService.scopedSpecification(currentSpecification, null, READ))
            .withMessage("'objectClass' must not be null");
   }

   @Test
   public void testScopedSpecificationBySpecificationWhenNullScopedContext() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      Specification<Registered> currentSpecification = __ -> true;

      assertThat(scopeService.scopedSpecification(currentSpecification, Registered.class, null)).isNotNull();
   }

   @Test
   public void testScopedSpecificationByCompositeSpecificationWhenNominal() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      CompositeSpecification<Registered> currentSpecification = __ -> true;
      CompositeSpecification<Registered> scopedSpecification =
            scopeService.scopedSpecification(currentSpecification, Registered.class, READ);

      assertThat(scopedSpecification).isNotNull();
      assertThat(scopedSpecification.satisfiedBy(new Registered())).isFalse();
   }

   @Test
   public void testScopedSpecificationByCompositeSpecificationWhenUnregisteredClass() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      CompositeSpecification<UnRegistered> currentSpecification = __ -> true;
      CompositeSpecification<UnRegistered> scopedSpecification =
            scopeService.scopedSpecification(currentSpecification, UnRegistered.class, READ);

      assertThat(scopedSpecification).isNotNull();
      assertThat(scopedSpecification.satisfiedBy(new UnRegistered())).isTrue();
   }

   @Test
   public void testScopedSpecificationByCompositeSpecificationWhenNullSpecification() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThatNullPointerException()
            .isThrownBy(() -> scopeService.scopedSpecification((CompositeSpecification<Registered>) null,
                                                               Registered.class,
                                                               READ))
            .withMessage("'specification' must not be null");
   }

   @Test
   public void testScopedSpecificationByCompositeSpecificationWhenNullObjectClass() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      CompositeSpecification<Registered> currentSpecification = __ -> true;

      assertThatNullPointerException()
            .isThrownBy(() -> scopeService.scopedSpecification(currentSpecification, null, READ))
            .withMessage("'objectClass' must not be null");
   }

   @Test
   public void testScopedSpecificationByCompositeSpecificationWhenNullScopedContext() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      CompositeSpecification<Registered> currentSpecification = __ -> true;

      assertThat(scopeService.scopedSpecification(currentSpecification, Registered.class, null)).isNotNull();
   }

   @Test
   public void testInScopeWhenNominal() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThat(scopeService.inScope(new Registered("inScope"), READ, false)).isTrue();
      assertThat(scopeService.inScope(new Registered("inScope"), READ, true)).isTrue();
      assertThat(scopeService.inScope(new Registered("notInScope"), READ, false)).isFalse();
      assertThat(scopeService.inScope(new Registered("notInScope"), READ, true)).isFalse();
   }

   @Test
   public void testInScopeWhenNullObject() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThat(scopeService.inScope((Registered) null, READ, false)).isTrue();
      assertThat(scopeService.inScope((Registered) null, READ, true)).isTrue();
      assertThat(scopeService.inScope((UnRegistered) null, READ, false)).isTrue();
      assertThat(scopeService.inScope((UnRegistered) null, READ, true)).isTrue();
   }

   @Test
   public void testInScopeWhenNullScopeContext() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThat(scopeService.inScope(new Registered("inScope"), null, false)).isTrue();
   }

   @Test
   public void testInScopeWhenOptionalObject() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThat(scopeService.inScope(optional(new Registered("inScope")), READ, false)).isTrue();
      assertThat(scopeService.inScope(optional(new Registered("inScope")), READ, true)).isTrue();
      assertThat(scopeService.inScope(optional(new Registered("notInScope")), READ, false)).isFalse();
      assertThat(scopeService.inScope(optional(new Registered("notInScope")), READ, true)).isFalse();
      assertThat(scopeService.inScope(optional(), READ, false)).isTrue();
      assertThat(scopeService.inScope(optional(), READ, true)).isTrue();
      assertThat(scopeService.inScope(optional(new UnRegistered()), READ, false)).isTrue();
      assertThatExceptionOfType(NotRegisteredScopeException.class)
            .isThrownBy(() -> scopeService.inScope(optional(new UnRegistered("any")), READ, true))
            .withMessage("Scope not registered for 'com.tinubu.wallkeeper.type.UnRegistered'");
      assertThat(scopeService.inScope((Optional<?>) null, READ, false)).isTrue();
      assertThat(scopeService.inScope((Optional<?>) null, READ, true)).isTrue();
   }

   @Test
   public void testInScopeWhenOptionalComposedObject() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThat(scopeService.inScope(optional(optional(new Registered("inScope"))), READ, false)).isTrue();
      assertThat(scopeService.inScope(optional(optional(new Registered("inScope"))), READ, true)).isTrue();
      assertThat(scopeService.inScope(optional(optional(new Registered("notInScope"))),
                                      READ,
                                      false)).isFalse();
      assertThat(scopeService.inScope(optional(optional(new Registered("notInScope"))),
                                      READ,
                                      true)).isFalse();
      assertThat(scopeService.inScope(optional(optional()), READ, false)).isTrue();
      assertThat(scopeService.inScope(optional(optional()), READ, true)).isTrue();
      assertThat(scopeService.inScope(optional(optional(new UnRegistered("any"))), READ, false)).isTrue();
      assertThatExceptionOfType(NotRegisteredScopeException.class)
            .isThrownBy(() -> scopeService.inScope(optional(optional(new UnRegistered("any"))), READ, true))
            .withMessage("Scope not registered for 'com.tinubu.wallkeeper.type.UnRegistered'");
      assertThat(scopeService.inScope((Optional<Optional<?>>) null, READ, false)).isTrue();
      assertThat(scopeService.inScope((Optional<Optional<?>>) null, READ, true)).isTrue();

      assertThat(scopeService.inScope(optional(Arrays.asList(new Registered("inScope"))),
                                      READ,
                                      false)).isTrue();
      assertThat(scopeService.inScope(optional(Arrays.asList(new Registered("inScope"))),
                                      READ,
                                      true)).isTrue();
      assertThat(scopeService.inScope(optional(Arrays.asList(new Registered("notInScope"))),
                                      READ,
                                      false)).isFalse();
      assertThat(scopeService.inScope(optional(Arrays.asList(new Registered("notInScope"))),
                                      READ,
                                      true)).isFalse();
      assertThat(scopeService.inScope(optional(Arrays.asList()), READ, false)).isTrue();
      assertThat(scopeService.inScope(optional(Arrays.asList()), READ, true)).isTrue();
      assertThat(scopeService.inScope(optional(Arrays.asList(new UnRegistered("any"))),
                                      READ,
                                      false)).isTrue();
      assertThatExceptionOfType(NotRegisteredScopeException.class)
            .isThrownBy(() -> scopeService.inScope(optional(Arrays.asList(new UnRegistered("any"))),
                                                   READ,
                                                   true))
            .withMessage("Scope not registered for 'com.tinubu.wallkeeper.type.UnRegistered'");
      assertThat(scopeService.inScope((Optional<List<?>>) null, READ, false)).isTrue();
      assertThat(scopeService.inScope((Optional<List<?>>) null, READ, true)).isTrue();
      assertThat(scopeService.inScope(optional(Arrays.asList(optional(Arrays.asList(new Registered("inScope"))))),
                                      READ,
                                      false)).isTrue();
   }

   @Test
   public void testInScopeWhenIterableObject() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThat(scopeService.inScope(Arrays.asList(new Registered("inScope")), READ, false)).isTrue();
      assertThat(scopeService.inScope(Arrays.asList(new Registered("inScope")), READ, true)).isTrue();
      assertThat(scopeService.inScope(Arrays.asList(new Registered("notInScope")), READ, false)).isFalse();
      assertThat(scopeService.inScope(Arrays.asList(new Registered("notInScope")), READ, true)).isFalse();
      assertThat(scopeService.inScope(Arrays.asList(new Registered("inScope"), new Registered("notInScope")),
                                      READ,
                                      false)).isFalse();
      assertThat(scopeService.inScope(Arrays.asList(new Registered("inScope"), new Registered("notInScope")),
                                      READ,
                                      true)).isFalse();
      assertThat(scopeService.inScope(Arrays.asList(), READ, false)).isTrue();
      assertThat(scopeService.inScope(Arrays.asList(), READ, true)).isTrue();
      assertThat(scopeService.inScope(Arrays.asList(new UnRegistered("any")), READ, false)).isTrue();
      assertThatExceptionOfType(NotRegisteredScopeException.class)
            .isThrownBy(() -> scopeService.inScope(Arrays.asList(new UnRegistered("any")), READ, true))
            .withMessage("Scope not registered for 'com.tinubu.wallkeeper.type.UnRegistered'");
      assertThat(scopeService.inScope(Arrays.asList(new Registered("inScope"), new UnRegistered("any")),
                                      READ,
                                      false)).isTrue();
      assertThatExceptionOfType(NotRegisteredScopeException.class)
            .isThrownBy(() -> scopeService.inScope(Arrays.asList(new Registered("inScope"),
                                                                 new UnRegistered("any")), READ, true))
            .withMessage("Scope not registered for 'com.tinubu.wallkeeper.type.UnRegistered'");
      assertThat(scopeService.inScope((List<?>) null, READ, false)).isTrue();
      assertThat(scopeService.inScope((List<?>) null, READ, true)).isTrue();
      assertThat(scopeService.inScope(Arrays.asList(null, null), READ, false)).isTrue();
      assertThat(scopeService.inScope(Arrays.asList(null, null), READ, true)).isTrue();
   }

   @Test
   public void testInScopeWhenIterableComposedObject() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThat(scopeService.inScope(Arrays.asList(new Registered("inScope"),
                                                    optional(new Registered("inScope")),
                                                    Arrays.asList(new Registered("inScope"))),
                                      READ,
                                      false)).isTrue();
      assertThat(scopeService.inScope(Arrays.asList(new Registered("inScope"),
                                                    optional(new Registered("inScope")),
                                                    Arrays.asList(new Registered("inScope"))),
                                      READ,
                                      true)).isTrue();
      assertThat(scopeService.inScope(Arrays.asList(new Registered("notInScope"),
                                                    optional(new Registered("notInScope")),
                                                    Arrays.asList(new Registered("notInScope"))),
                                      READ,
                                      false)).isFalse();
      assertThat(scopeService.inScope(Arrays.asList(new Registered("notInScope"),
                                                    optional(new Registered("notInScope")),
                                                    Arrays.asList(new Registered("notInScope"))),
                                      READ,
                                      true)).isFalse();
      assertThat(scopeService.inScope(Arrays.asList(new Registered("inScope"),
                                                    optional(new Registered("inScope")),
                                                    Arrays.asList(new Registered("notInScope"))),
                                      READ,
                                      false)).isFalse();
      assertThat(scopeService.inScope(Arrays.asList(new Registered("inScope"),
                                                    optional(new Registered("inScope")),
                                                    Arrays.asList(new Registered("notInScope"))),
                                      READ,
                                      true)).isFalse();
      assertThat(scopeService.inScope(Arrays.asList(optional(), Arrays.asList()), READ, false)).isTrue();
      assertThat(scopeService.inScope(Arrays.asList(optional(), Arrays.asList()), READ, true)).isTrue();
      assertThat(scopeService.inScope(Arrays.asList(new UnRegistered("any"),
                                                    optional(new UnRegistered("any")),
                                                    Arrays.asList(new UnRegistered("any"))),
                                      READ,
                                      false)).isTrue();
      assertThatExceptionOfType(NotRegisteredScopeException.class)
            .isThrownBy(() -> scopeService.inScope(Arrays.asList(new UnRegistered("any"),
                                                                 optional(new UnRegistered("any")),
                                                                 Arrays.asList(new UnRegistered("any"))),
                                                   READ,
                                                   true))
            .withMessage("Scope not registered for 'com.tinubu.wallkeeper.type.UnRegistered'");
      assertThat(scopeService.inScope(Arrays.asList(new Registered("inScope"),
                                                    optional(new Registered("inScope")),
                                                    Arrays.asList(new Registered("inScope"),
                                                                  new UnRegistered("any"))),
                                      READ,
                                      false)).isTrue();
      assertThatExceptionOfType(NotRegisteredScopeException.class)
            .isThrownBy(() -> scopeService.inScope(Arrays.asList(new Registered("inScope"),
                                                                 optional(new Registered("inScope")),
                                                                 Arrays.asList(new Registered("inScope"),
                                                                               new UnRegistered("any"))),
                                                   READ,
                                                   true))
            .withMessage("Scope not registered for 'com.tinubu.wallkeeper.type.UnRegistered'");
      assertThat(scopeService.inScope(Arrays.asList(new Registered("inScope"),
                                                    optional(new Registered("inScope")),
                                                    Arrays.asList(null, null)), READ, false)).isTrue();
      assertThat(scopeService.inScope(Arrays.asList(new Registered("inScope"),
                                                    optional(new Registered("inScope")),
                                                    Arrays.asList(null, null)), READ, true)).isTrue();
      assertThat(scopeService.inScope(Arrays.asList(optional(Arrays.asList(optional(new Registered("inScope"))))),
                                      READ,
                                      false)).isTrue();
   }

   @Test
   public void testInScopeWhenUnregisteredClass() {
      ScopeService<ScopeContext, UserScope> scopeService =
            new ScopeService<>(() -> optional(TEST_USER_SCOPE), mockScopeFactory());

      assertThat(scopeService.inScope(new UnRegistered("any"), READ, false)).isTrue();
      assertThatExceptionOfType(NotRegisteredScopeException.class)
            .isThrownBy(() -> scopeService.inScope(new UnRegistered("any"), READ, true))
            .withMessage("Scope not registered for 'com.tinubu.wallkeeper.type.UnRegistered'");
   }

   @SuppressWarnings("unchecked")
   private ScopeFactory<ScopeContext, UserScope> mockScopeFactory() {
      ScopeFactory<ScopeContext, UserScope> mock = mock(ScopeFactory.class);

      when(mock.scope(Registered.class, READ, optional(TEST_USER_SCOPE))).thenReturn(optional(new Registered(
            "inScope")::equals));
      when(mock.scope(Registered.class, null, optional(TEST_USER_SCOPE))).thenReturn(optional(new Registered(
            "inScope")::equals));

      return mock;
   }

}