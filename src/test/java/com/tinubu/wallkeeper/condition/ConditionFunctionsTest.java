/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.wallkeeper.condition;

import static com.tinubu.wallkeeper.condition.ConditionFunctions.hasEnvironment;
import static com.tinubu.wallkeeper.condition.ConditionFunctions.hasProperty;
import static com.tinubu.wallkeeper.condition.ConditionFunctions.hasSpringEnvironment;
import static com.tinubu.wallkeeper.condition.ConditionFunctions.hasSpringProfile;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

@ContextConfiguration(classes = ConditionFunctionsTest.class)
@TestPropertySource(properties = { "test.property=match" })
public class ConditionFunctionsTest extends AbstractScopeConditionTest {

   @Test
   public void testHasPropertyWhenNominal() {
      System.setProperty("test.property", "match");

      assertThat(hasProperty("test.property", ".*")).isTrue();
   }

   @Test
   public void testHasPropertyWhenBadParameters() {
      assertThatNullPointerException()
            .isThrownBy(() -> hasProperty(null, "matches"))
            .withMessage("'named' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> hasProperty("", "matches"))
            .withMessage("'named' must not be blank");
      assertThatNullPointerException()
            .isThrownBy(() -> hasProperty("named", null))
            .withMessage("'matches' must not be null");
   }

   @Test
   public void testHasPropertyWhenNoMatch() {
      System.setProperty("test.property", "match");

      assertThat(hasProperty("test.property", "nomatch")).isFalse();
   }

   @Test
   public void testHasPropertyWhenMissing() {
      assertThat(hasProperty("missing.property", "match", true)).isTrue();
      assertThat(hasProperty("missing.property", "match", false)).isFalse();
   }

   @Test
   public void testHasEnvironmentWhenNominal() {
      assertThat(hasEnvironment("HOME", ".*")).isTrue();
   }

   @Test
   public void testHasEnvironmentWhenBadParameters() {
      assertThatNullPointerException()
            .isThrownBy(() -> hasEnvironment(null, "matches"))
            .withMessage("'named' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> hasEnvironment("", "matches"))
            .withMessage("'named' must not be blank");
      assertThatNullPointerException()
            .isThrownBy(() -> hasEnvironment("named", null))
            .withMessage("'matches' must not be null");
   }

   @Test
   public void testHasEnvironmentWhenNoMatch() {
      assertThat(hasEnvironment("HOME", "nomatch")).isFalse();
   }

   @Test
   public void testHasEnvironmentWhenMissing() {
      assertThat(hasEnvironment("MISSING", "match", true)).isTrue();
      assertThat(hasEnvironment("MISSING", "match", false)).isFalse();
   }

   @Test
   public void testHasSpringEnvironmentWhenNominal() {
      assertThat(hasProperty("test.property", ".*")).isTrue();
   }

   @Test
   public void testHasSpringEnvironmentWhenBadParameters() {
      assertThatNullPointerException()
            .isThrownBy(() -> hasSpringEnvironment(null, "matches"))
            .withMessage("'named' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> hasSpringEnvironment("", "matches"))
            .withMessage("'named' must not be blank");
      assertThatNullPointerException()
            .isThrownBy(() -> hasSpringEnvironment("named", null))
            .withMessage("'matches' must not be null");
   }

   @Test
   public void testHasSpringEnvironmentWhenNoMatch() {
      assertThat(hasProperty("test.property", "nomatch")).isFalse();
   }

   @Test
   public void testHasSpringEnvironmentWhenMissing() {
      assertThat(hasProperty("missing.property", "match", true)).isTrue();
      assertThat(hasProperty("missing.property", "match", false)).isFalse();
   }

   @Test
   public void testHasSpringProfileWhenNominal() {
      assertThat(hasSpringProfile("test")).isTrue();
   }

   @Test
   public void testHasSpringProfileWhenBadParameters() {
      assertThatNullPointerException()
            .isThrownBy(() -> hasSpringProfile(null))
            .withMessage("'named' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> hasSpringProfile(""))
            .withMessage("'named' must not be blank");
   }

   @Test
   public void testHasSpringProfileWhenMissing() {
      assertThat(hasSpringProfile("missing")).isFalse();
   }

}
